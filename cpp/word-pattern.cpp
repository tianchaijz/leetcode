#include "common.h"

class Solution {
  public:
    bool wordPattern(string pattern, string str) {
        if (pattern.size() != static_cast<size_t>(std::count(str.begin(), str.end(), ' ')) + 1)
            return false;

        map<char, int> mp;
        map<string, int> ms;
        size_t idx = 0;
        for (size_t i = 0; i < pattern.size(); ++i) {
            size_t j = str.find(' ', idx);
            string w = str.substr(idx, j == string::npos ? j : j - idx);
            if (mp[pattern[i]] != ms[w])
                return false;

            mp[pattern[i]] = ms[w] = i + 1;
            idx = j + 1;
        }

        return true;
    }
};

class Solution1 {
  public:
    bool wordPattern(string pattern, string str) {
        map<char, int> c2i;
        map<string, int> w2i;

        std::istringstream in(str);
        size_t i = 0, n = pattern.size();
        for (string w; in >> w; ++i) {
            if (i == n || c2i[pattern[i]] != w2i[w])
                return false;

            c2i[pattern[i]] = w2i[w] = i + 1;
        }

        return i == n;
    }
};

TEST_CASE {
    Solution s;

    assert(s.wordPattern("ab", "dog cat cat dog") == false);
    assert(s.wordPattern("abba", "dog cat cat dog") == true);
    assert(s.wordPattern("abba", "dog cat cat fish") == false);
}

int main(int argc, char *argv[]) {
    TEST(Solution);
    TEST(Solution1);

    return 0;
}
