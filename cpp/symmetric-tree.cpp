#include "common.h"

class Solution {
  public:
    bool isSymmetric(TreeNode *root) {
        if (root == nullptr)
            return true;
        return symmetric(root->left, root->right);
    }

  private:
    bool symmetric(TreeNode *left, TreeNode *right) {
        if (left == nullptr && right == nullptr)
            return true;
        if (left == nullptr || right == nullptr)
            return false;

        return left->val == right->val && symmetric(left->left, right->right) && symmetric(left->right, right->left);
    }
};

class Solution1 {
  public:
    bool isSymmetric(TreeNode *root) {
        if (root == nullptr)
            return true;

        stack<TreeNode *> s;
        s.push(root->left);
        s.push(root->right);
        while (!s.empty()) {
            auto r = s.top();
            s.pop();
            auto l = s.top();
            s.pop();

            if (r == nullptr && l == nullptr)
                continue;
            if (r == nullptr || l == nullptr)
                return false;
            if (r->val != l->val)
                return false;

            s.push(r->left);
            s.push(l->right);

            s.push(r->right);
            s.push(l->left);
        }

        return true;
    }
};

class Solution2 {
  public:
    bool isSymmetric(TreeNode *root) {
        if (root == nullptr)
            return true;

        queue<TreeNode *> q;
        q.push(root->left);
        q.push(root->right);
        while (!q.empty()) {
            auto l = q.front();
            q.pop();
            auto r = q.front();
            q.pop();

            if (l == nullptr && r == nullptr)
                continue;
            if (l == nullptr || r == nullptr)
                return false;
            if (l->val != r->val)
                return false;

            q.push(l->left);
            q.push(r->right);

            q.push(l->right);
            q.push(r->left);
        }

        return true;
    }
};

TEST_CASE {
    Solution s;
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
