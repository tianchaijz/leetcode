#include "common.h"

class Queue {
  public:
    void push(int x) {
        input_.push(x);
    }

    void pop(void) {
        barrier();
        output_.pop();
    }

    int peek(void) {
        barrier();
        return output_.top();
    }

    bool empty(void) {
        return input_.empty() && output_.empty();
    }

  private:
    void barrier() {
        if (output_.empty()) {
            while (!input_.empty()) {
                output_.push(input_.top());
                input_.pop();
            }
        }
    }

    stack<int> input_, output_;
};

TEST_CASE {
    Solution s;
}

int main(int argc, char *argv[]) {
    TEST(Queue);

    return 0;
}
