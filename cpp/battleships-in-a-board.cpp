#include "common.h"

class Solution {
  public:
    int countBattleships(vector<vector<char>> &board) {
        size_t row = board.size();
        if (row == 0)
            return 0;
        int count = 0;
        size_t col = board[0].size();
        for (size_t i = 0; i < row; ++i) {
            for (size_t j = 0; j < col; ++j) {
                if (board[i][j] == '.')
                    continue;
                if (i > 0 && board[i - 1][j] == 'X')
                    continue;
                if (j > 0 && board[i][j - 1] == 'X')
                    continue;
                ++count;
            }
        }

        return count;
    }
};

TEST_CASE {
    Solution s;
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
