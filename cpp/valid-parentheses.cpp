#include "common.h"

// Valid Parentheses
class Solution {
  public:
    bool isValid(string s) {
        stack<char> stack;
        for (auto &c : s) {
            switch (c) {
            case ')':
                if (stack.empty() || stack.top() != '(')
                    return false;
                stack.pop();
                break;
            case ']':
                if (stack.empty() || stack.top() != '[')
                    return false;
                stack.pop();
                break;
            case '}':
                if (stack.empty() || stack.top() != '{')
                    return false;
                stack.pop();
                break;
            default:
                stack.push(c);
                break;
            }
        }

        return stack.empty();
    }
};

TEST_CASE {
    Solution s;

    assert(s.isValid("{()}") == true);
    assert(s.isValid("(()") == false);
    assert(s.isValid("(") == false);
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
