#include "common.h"

class Solution {
  public:
    bool isIsomorphic(string s, string t) {
        if (s.size() != t.size())
            return false;
        size_t ms[256] = {}, mt[256] = {};
        for (size_t i = 0; i < s.size(); ++i) {
            if (ms[s[i] - '\0'] != mt[t[i] - '\0'])
                return false;
            ms[s[i] - '\0'] = mt[t[i] - '\0'] = i + 1;
        }

        return true;
    }
};

TEST_CASE {
    Solution s;

    assert(s.isIsomorphic("aba", "baa") == false);
    assert(s.isIsomorphic("foo", "bar") == false);
    assert(s.isIsomorphic("egg", "add") == true);
    assert(s.isIsomorphic("paper", "title") == true);
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
