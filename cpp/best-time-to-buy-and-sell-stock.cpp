#include "common.h"

class Solution {
  public:
    int maxProfit(vector<int> &prices) {
        int x = INT_MAX, y = 0;
        for (int price : prices) {
            x = min(price, x);
            y = max(price - x, y);
        }

        return y;
    }
};

TEST_CASE {
    Solution s;
    vector<int> v1 = {7, 1, 5, 3, 6, 4};
    vector<int> v2 = {7, 6, 4, 3, 1};

    assert(s.maxProfit(v1) == 5);
    assert(s.maxProfit(v2) == 0);
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
