#include "common.h"

class Solution {
  public:
    int islandPerimeter(vector<vector<int>> &grid) {
        int l = grid.size(), w = grid[0].size();
        int r = 0;
        for (int row = 0; row < l; ++row) {
            for (int col = 0; col < w; ++col) {
                if (grid[row][col]) {
                    r += 4;
                    if (row > 0 && grid[row - 1][col])
                        r -= 2;
                    if (col > 0 && grid[row][col - 1])
                        r -= 2;
                }
            }
        }

        return r;
    }
};

class Solution1 {
  public:
    int islandPerimeter(vector<vector<int>> &grid) {
        int r = 0;
        for (int i = 0; i < grid.size(); ++i) {
            for (int j = 0; j < grid[0].size(); ++j) {
                if (grid[i][j]) {
                    dfs(grid, i, j, r);
                    return r;
                }
            }
        }

        std::cout << r << std::endl;
        return r;
    }

  private:
    void dfs(vector<vector<int>> &grid, int i, int j, int &r) {
        if (i < 0 || i >= grid.size() || j < 0 || j >= grid[0].size() || grid[i][j] != 1)
            return;

        grid[i][j] = -1;

        if (i - 1 < 0 || grid[i - 1][j] == 0)
            r++;
        if (j - 1 < 0 || grid[i][j - 1] == 0)
            r++;
        if (i + 1 >= grid.size() || grid[i + 1][j] == 0)
            r++;
        if (j + 1 >= grid[0].size() || grid[i][j + 1] == 0)
            r++;

        dfs(grid, i - 1, j, r);
        dfs(grid, i + 1, j, r);
        dfs(grid, i, j - 1, r);
        dfs(grid, i, j + 1, r);
    }
};

TEST_CASE {
    Solution s;
    vector<vector<int>> grid{{0, 1, 0, 0}, {1, 1, 1, 0}, {0, 1, 0, 0}, {1, 1, 0, 0}};

    assert(s.islandPerimeter(grid) == 16);
}

int main(int argc, char *argv[]) {
    TEST(Solution);
    TEST(Solution1);

    return 0;
}
