#include "common.h"

class Solution {
  public:
    string frequencySort(string s) {
        unordered_map<char, int> m;
        vector<vector<char>> v;
        int mf = 0;
        string r;

        for (char c : s)
            mf = max(++m[c], mf);
        v.resize(mf + 1);
        for (auto &x : m)
            v[x.second].push_back(x.first);
        for (int i = mf; i > 0; --i) {
            if (v[i].empty())
                continue;
            sort(v[i].begin(), v[i].end());
            for (char c : v[i])
                r += string(i, c);
        }

        return r;
    }
};

class Solution1 {
  public:
    string frequencySort(string s) {
        unordered_map<char, int> m;
        vector<string> bucket;
        int mf = 0;
        string r;

        for (char c : s)
            mf = max(++m[c], mf);
        bucket.resize(mf + 1);
        for (auto &x : m) {
            int n = x.second;
            bucket[n].append(n, x.first);
        }
        for (int i = mf; i > 0; --i) {
            if (!bucket[i].empty())
                r.append(bucket[i]);
        }

        return r;
    }
};

TEST_CASE {
    Solution s;

    assert(s.frequencySort("tree") == "eert" ||
           s.frequencySort("tree") == "eetr");
    assert(s.frequencySort("accc") == "ccca");
    assert(s.frequencySort("Aabb") == "bbAa" ||
           s.frequencySort("Aabb") == "bbaA");
}

int main(int argc, char *argv[]) {
    TEST(Solution);
    TEST(Solution1);

    return 0;
}
