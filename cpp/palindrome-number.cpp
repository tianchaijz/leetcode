#include "common.h"

class Solution {
  public:
    bool isPalindrome(int x) {
        if (x < 0 || (x != 0 && x % 10 == 0))
            return false;
        int sum = 0;
        while (x > sum) {
            sum = sum * 10 + x % 10;
            x /= 10;
        }

        return x == sum || x == sum / 10;
    }
};

TEST_CASE {
    Solution s;

    assert(s.isPalindrome(1) == true);
    assert(s.isPalindrome(10) == false);
    assert(s.isPalindrome(11) == true);
    assert(s.isPalindrome(121) == true);
    assert(s.isPalindrome(1221) == true);
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
