#include "common.h"

class Solution {
  public:
    bool isPowerOfThree(int n) {
        while (n > 0 && n % 3 == 0) {
            n /= 3;
        }

        return n == 1;
    }
};

class Solution1 {
  public:
    bool isPowerOfThree(int n) {
        return n > 0 && int(pow(3, int(log(INT_MAX) / log(3)))) % n == 0;
    }
};

TEST_CASE {
    Solution s;
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
