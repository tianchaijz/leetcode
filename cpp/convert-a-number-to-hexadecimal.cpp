#include "common.h"

class Solution {
  public:
    string toHex(int num) {
        if (num == 0)
            return "0";

        string r;
        unsigned x = num;
        for (; x; x >>= 4) {
            r += hex[x & 0xf];
        }

        reverse(r.begin(), r.end());

        return r;
    }

  private:
    const string hex = "0123456789abcdef";
};

TEST_CASE {
    Solution s;

    assert(s.toHex(0) == "0");
    assert(s.toHex(26) == "1a");
    assert(s.toHex(-1) == "ffffffff");
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
