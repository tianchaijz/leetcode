#include "common.h"

class Solution {
  public:
    bool isAnagram(string s, string t) {
        int c[256] = {0};
        for (unsigned char a : s)
            ++c[a];
        for (unsigned char a : t)
            --c[a];
        for (unsigned char a : c)
            if (a)
                return false;

        return true;
    }
};

TEST_CASE {
    Solution s;

    assert(s.isAnagram("anagram", "nagaram") == true);
    assert(s.isAnagram("ab", "a") == false);
    assert(s.isAnagram("a", "ab") == false);
    assert(s.isAnagram("rat", "car") == false);
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
