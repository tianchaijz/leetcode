#include "common.h"

class Solution {
  public:
    int computeArea(int A, int B, int C, int D, int E, int F, int G, int H) {
        if (A > C)
            swap(A, C);
        if (B > D)
            swap(B, D);
        if (E > G)
            swap(E, G);
        if (F > H)
            swap(F, H);

        int total = (C - A) * (D - B) + (G - E) * (H - F);
        if (A >= G || B >= H || C <= E || D <= F)
            return total;

        vector<int> x{A, C, E, G};
        vector<int> y{B, D, F, H};

        sort(x.begin(), x.end());
        sort(y.begin(), y.end());

        int overlap = (x[2] - x[1]) * (y[2] - y[1]);

        return total - overlap;
    }
};

TEST_CASE {
    Solution s;

    assert(s.computeArea(-3, 0, 3, 4, 0, -1, 9, 2) == 45);
    assert(s.computeArea(0, 0, 0, 0, -1, -1, 1, 1) == 4);
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
