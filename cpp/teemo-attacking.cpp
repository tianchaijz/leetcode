#include "common.h"

class Solution {
  public:
    int findPoisonedDuration(vector<int> &timeSeries, int duration) {
        if (timeSeries.empty() || duration <= 0)
            return 0;
        int t = duration;
        for (size_t i = 1; i < timeSeries.size(); ++i) {
            t += min(timeSeries[i] - timeSeries[i - 1], duration);
        }

        return t;
    }
};

TEST_CASE {
    Solution s;

    vector<int> v1{1, 4}, v2{1, 3}, v3{1, 2};

    assert(s.findPoisonedDuration(v1, 2) == 4);
    assert(s.findPoisonedDuration(v2, 2) == 4);
    assert(s.findPoisonedDuration(v3, 2) == 3);

    assert(s.findPoisonedDuration(v1, 3) == 6);
    assert(s.findPoisonedDuration(v2, 3) == 5);
    assert(s.findPoisonedDuration(v3, 3) == 4);
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
