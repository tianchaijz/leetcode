#include "common.h"

class Solution {
  public:
    bool hasPathSum(TreeNode *root, int sum) {
        if (root == nullptr)
            return false;

        if (root->left == nullptr && root->right == nullptr)
            return root->val == sum;

        int remain = sum - root->val;
        return hasPathSum(root->left, remain) || hasPathSum(root->right, remain);
    }
};

TEST_CASE {
    Solution s;
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
