#include "common.h"

// https://discuss.leetcode.com/topic/12910/my-easy-one-round-c-code
class Solution {
  public:
    int countPrimes(int n) {
        if (n < 3)
            return 0;
        vector<bool> passed(n, false);
        int count = 1;
        int upper = sqrt(n);
        for (int i = 3; i < n; i += 2) {
            if (!passed[i]) {
                ++count;
                if (i > upper)
                    continue;
                for (int j = i * i; j < n; j += i) {
                    passed[j] = true;
                }
            }
        }

        return count;
    }
};

TEST_CASE {
    Solution s;

    assert(s.countPrimes(5) == 2);
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
