#include "common.h"

class Solution {
  public:
    int longestPalindrome(string s) {
        int c[256] = {0};
        int r = 0;
        for (unsigned char a : s)
            ++c[a];
        for (int i : c)
            r += i & 1 ? i - 1 : i;

        return r < s.size() ? r + 1 : r;
    }
};

TEST_CASE {
    Solution s;

    assert(s.longestPalindrome("a") == 1);
    assert(s.longestPalindrome("aa") == 2);
    assert(s.longestPalindrome("aaa") == 3);
    assert(s.longestPalindrome("abccccdd") == 7);
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
