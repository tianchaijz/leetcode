#include "common.h"

class Solution {
  public:
    int rob(vector<int> &nums) {
        int a = 0, b = 0;
        for (int i = 0; i < nums.size(); ++i) {
            int c = max(b, a + nums[i]);
            a = b;
            b = c;
        }

        return b;
    }
};

TEST_CASE {
    Solution s;
    vector<int> v1 = {};
    vector<int> v2 = {1};
    vector<int> v3 = {1, 2};
    vector<int> v4 = {1, 2, 3};
    vector<int> v5 = {1, 2, 3, 4};
    vector<int> v6 = {1, 2, 3, 4, 5};

    assert(s.rob(v1) == 0);
    assert(s.rob(v2) == 1);
    assert(s.rob(v3) == 2);
    assert(s.rob(v4) == 4);
    assert(s.rob(v5) == 6);
    assert(s.rob(v6) == 9);
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
