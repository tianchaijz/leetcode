#include "common.h"

class Solution {
  public:
    int minDepth(TreeNode *root) {
        if (root == nullptr)
            return 0;
        if (root->left == nullptr)
            return 1 + minDepth(root->right);
        else if (root->right == nullptr)
            return 1 + minDepth(root->left);
        else
            return 1 + min(minDepth(root->left), minDepth(root->right));
    }
};

TEST_CASE {
    Solution s;

    TreeNode a(1);
    TreeNode b(1, &a, nullptr);

    assert(s.minDepth(&a) == 1);
    assert(s.minDepth(&b) == 2);
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
