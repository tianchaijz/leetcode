#define TEST_CALLS 1
#include "common.h"

// https://discuss.leetcode.com/topic/9801/summary-of-c-solutions/2
class Solution {
  public:
    void rotate(vector<int> &nums, int k) {
        int n = nums.size();
        k = n - k % n;

        std::reverse(nums.begin(), nums.begin() + k);
        std::reverse(nums.begin() + k, nums.end());
        std::reverse(nums.begin(), nums.end());
    }
};

class Solution1 {
  public:
    void rotate(vector<int> &nums, int k) {
        int n = nums.size();
        auto iter = nums.begin();
        for (; (k = k % n); n -= k, iter += k) {
            // Swap the last k elements with the first k elements.
            // The last k elements will be in the correct positions
            // but we need to rotate the remaining (n - k) elements
            // to the right by k steps.
            for (int i = 0; i < k; i++) {
                swap(iter[i], iter[n - k + i]);
            }
        }
    }
};

TEST_CASE {
    Solution s;

    vector<int> v{1, 2, 3, 4, 5, 6, 7};

    s.rotate(v, 3);
    assert(v == (vector<int>{5, 6, 7, 1, 2, 3, 4}));
}

int main(int argc, char *argv[]) {
    TEST(Solution);
    TEST(Solution1);

    return 0;
}
