#include "common.h"

class Solution {
  public:
    void moveZeroes(vector<int> &nums) {
        int i = 0;
        for (int j = 0; j < nums.size(); ++j) {
            if (nums[j]) {
                int t = nums[i];
                nums[i++] = nums[j];
                nums[j] = t;
            }
        }
    }
};

class Solution1 {
  public:
    void moveZeroes(vector<int> &nums) {
        int i = 0;
        for (auto n : nums)
            if (n)
                nums[i++] = n;
        fill(nums.begin() + i, nums.end(), 0);
    }
};

class Solution2 {
  public:
    void moveZeroes(vector<int> &nums) {
        int i = 0, j = 0;
        for (;;) {
            while (i < nums.size() && nums[i])
                ++i;
            if (i == nums.size())
                return;

            j = i + 1;
            while (j < nums.size() && nums[j] == 0)
                ++j;
            if (j == nums.size())
                return;

            swap(nums[i], nums[j]);
            ++i;
        }
    }
};

class Solution3 {
  public:
    void moveZeroes(vector<int> &nums) {
        for (int i = 0, j = 0; i < nums.size(); ++i) {
            if (nums[i] && j == i)
                ++j; // leading nums[i] is not zero, just skip
            else if (nums[i])
                swap(nums[i], nums[j++]);
        }
    }
};

TEST_CASE {
    Solution s;
    vector<int> v1{0, 1, 0, 3, 12};
    vector<int> v2{1};
    vector<int> v3{0, 1};

    s.moveZeroes(v1);
    s.moveZeroes(v2);
    s.moveZeroes(v3);

    assert(v1 == (vector<int>{1, 3, 12, 0, 0}));
    assert(v2 == (vector<int>{1}));
    assert(v3 == (vector<int>{1, 0}));
}

int main(int argc, char *argv[]) {
    TEST(Solution);
    TEST(Solution1);
    TEST(Solution2);
    TEST(Solution3);

    return 0;
}
