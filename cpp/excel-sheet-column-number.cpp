#include "common.h"

// Excel Sheet Column Number
class Solution {
  public:
    int titleToNumber(string s) {
        int r = 0;
        for (char c : s) {
            r = r * 26 + c - 'A' + 1;
        }

        return r;
    }
};

TEST_CASE {
    Solution s;

    assert(s.titleToNumber("A") == 1);
    assert(s.titleToNumber("B") == 2);
    assert(s.titleToNumber("Z") == 26);
    assert(s.titleToNumber("AA") == 27);
    assert(s.titleToNumber("AB") == 28);
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
