#include "common.h"

class Solution {
  public:
    bool isPalindrome(string s) {
        for (int lo = 0, hi = s.size() - 1; lo < hi; ++lo, --hi) {
            for (; lo < hi && !isalnum(s[lo]); ++lo)
                ;
            for (; lo < hi && !isalnum(s[hi]); --hi)
                ;
            if (lo < hi && std::tolower(s[lo]) != std::tolower(s[hi]))
                return false;
        }

        return true;
    }
};

TEST_CASE {
    Solution s;

    assert(s.isPalindrome(""));
    assert(s.isPalindrome("0a0"));
    assert(s.isPalindrome("A man, a plan, a canal: Panama"));
    assert(!s.isPalindrome("race a car"));
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
