#include "common.h"

class Solution {
  public:
    vector<int> twoSum(vector<int> &nums, int target) {
        vector<int> idx(nums.size());
        iota(idx.begin(), idx.end(), 0);
        sort(idx.begin(), idx.end(), [&](int x, int y) { return nums[x] < nums[y]; });

        vector<int> result(2, -1);
        for (int i = 0, j = nums.size() - 1; i < j; i++) {
            while (j > i + 1 && nums[idx[i]] + nums[idx[j]] > target)
                j--;

            int m = idx[i], n = idx[j];
            if (nums[m] + nums[n] == target) {
                if (m > n)
                    swap(m, n);
                result[0] = m;
                result[1] = n;
                break;
            }
        }

        return result;
    }
};

class Solution1 {
  public:
    vector<int> twoSum(vector<int> &nums, int target) {
        unordered_map<int, int> hash;
        for (int i = 0; i < nums.size(); i++) {
            auto it = hash.find(target - nums[i]);
            if (it != hash.end()) {
                if (i > it->second)
                    return vector<int>{it->second, i};
                return vector<int>{i, it->second};
            }

            hash[nums[i]] = i;
        }

        return vector<int>{-1, -1};
    }
};

class Solution2 {
  public:
    class Elem {
      public:
        int val;
        int idx;
        Elem(int v, int i) : val(v), idx(i) {
        }
        bool operator<(const Elem &e) const {
            return val < e.val;
        }
    };

    vector<int> twoSum(vector<int> &nums, int target) {
        vector<Elem> elems;
        for (int i = 0; i < nums.size(); i++)
            elems.push_back(Elem(nums[i], i));

        sort(elems.begin(), elems.end());

        vector<int> result(2, -1);
        int lo = 0, hi = nums.size() - 1;
        while (lo < hi) {
            int val = elems[lo].val + elems[hi].val;
            if (val == target) {
                result[0] = elems[lo].idx;
                result[1] = elems[hi].idx;
                if (result[0] > result[1]) {
                    swap(result[0], result[1]);
                }

                return result;
            } else if (val < target) {
                lo++;
            } else {
                hi--;
            }
        }

        return result;
    }
};

TEST_CASE {
    Solution s;
    vector<int> nums = {2, 7, 11, 15};

    assert(s.twoSum(nums, 9) == (vector<int>{0, 1}));
    assert(s.twoSum(nums, 13) == (vector<int>{0, 2}));
    assert(s.twoSum(nums, 18) == (vector<int>{1, 2}));
    assert(s.twoSum(nums, 17) == (vector<int>{0, 3}));
    assert(s.twoSum(nums, 20) == (vector<int>{-1, -1}));
}

int main(int argc, char *argv[]) {
    TEST(Solution);
    TEST(Solution1);
    TEST(Solution2);

    return 0;
}
