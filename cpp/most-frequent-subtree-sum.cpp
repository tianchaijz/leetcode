#include "common.h"

// https://discuss.leetcode.com/topic/77763/short-clean-c-o-n-solution
class Solution {
  public:
    vector<int> findFrequentTreeSum(TreeNode *root) {
        unordered_map<int, int> counts;
        vector<int> maxSums;
        int maxCount = 0;

        countSubtreeSums(root, counts, maxCount);
        for (auto &c : counts) {
            if (c.second == maxCount)
                maxSums.push_back(c.first);
        }

        return maxSums;
    }

  private:
    int countSubtreeSums(TreeNode *root, unordered_map<int, int> &counts,
                         int &maxCount) {
        if (root == nullptr)
            return 0;
        int sum = root->val;
        sum += countSubtreeSums(root->left, counts, maxCount) +
               countSubtreeSums(root->right, counts, maxCount);
        ++counts[sum];
        maxCount = max(maxCount, counts[sum]);

        return sum;
    }
};

TEST_CASE {
    Solution s;

    TreeNode a(2), b(-3), c(5, &a, &b), d(-5);

    vector<int> r = s.findFrequentTreeSum(&c);
    sort(r.begin(), r.end());
    assert(r == (vector<int>{-3, 2, 4}));

    c.right = &d;
    r = s.findFrequentTreeSum(&c);
    sort(r.begin(), r.end());
    assert(r == (vector<int>{2}));
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
