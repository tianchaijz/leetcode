#include "common.h"

struct Interval {
    int start;
    int end;
    Interval() : start(0), end(0) {
    }
    Interval(int s, int e) : start(s), end(e) {
    }
};

class Solution {
  public:
    vector<Interval> merge(vector<Interval> &intervals) {
        vector<Interval> r;

        if (intervals.empty())
            return r;
        sort(intervals.begin(), intervals.end(),
             [](Interval a, Interval b) { return a.start < b.start; });

        r.push_back(intervals[0]);
        for (size_t i = 1; i < intervals.size(); ++i) {
            if (r.back().end < intervals[i].start)
                r.push_back(intervals[i]);
            else
                r.back().end = max(r.back().end, intervals[i].end);
        }

        return r;
    }
};

TEST_CASE {
    Solution s;
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
