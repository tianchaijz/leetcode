#include "common.h"

// Hamming Distance
class Solution {
  public:
    int hammingDistance(int x, int y) {
        x ^= y;
        int r = 0;
        while (x > 0) {
            if (x & 1)
                ++r;
            x >>= 1;
        }
        return r;
    }
};

class Solution1 {
  public:
    int hammingDistance(int x, int y) {
        return __builtin_popcount(x ^ y);
    }
};

TEST_CASE {
    Solution s;

    assert(s.hammingDistance(1, 4) == 2);
}

int main(int argc, char *argv[]) {
    TEST(Solution);
    TEST(Solution1);

    return 0;
}
