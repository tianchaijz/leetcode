#include "common.h"

class Solution {
  public:
    ListNode *reverseList(ListNode *head) {
        ListNode *h = NULL, *t;
        while (head) {
            t = head->next;
            head->next = h;
            h = head;
            head = t;
        }

        return h;
    }
};

TEST_CASE {
    Solution s;
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
