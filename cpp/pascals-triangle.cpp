#define TEST_CALLS 1
#include "common.h"

class Solution {
  public:
    vector<vector<int>> generate(int numRows) {
        vector<vector<int>> r(numRows);
        for (int i = 0; i < numRows; ++i) {
            r[i].resize(i + 1);
            r[i][0] = r[i][i] = 1;

            for (int j = 1; j < i; ++j) {
                r[i][j] = r[i - 1][j - 1] + r[i - 1][j];
            }
        }

        return r;
    }
};

TEST_CASE {
    Solution s;

    auto p = s.generate(6);
    for (auto v : p)
        std::cout << v << std::endl;
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
