#define TEST_CALLS 1
#include "common.h"

/*
Given:
         1
        / \
       2   5
      / \   \
     3   4   6
The flattened tree should look like:
   1
    \
     2
      \
       3
        \
         4
          \
           5
            \
             6
*/
class Solution {
  public:
    void flatten(TreeNode *root) {
        if (root == nullptr)
            return;
        TreeNode *tail = nullptr;
        stack<TreeNode *> s;
        s.push(root);
        while (!s.empty()) {
            auto node = s.top();
            s.pop();
            if (tail)
                tail->right = node;
            tail = node;
            if (node->right)
                s.push(node->right);
            if (node->left) {
                s.push(node->left);
                node->left = nullptr;
            }
        }
        tail->right = nullptr;
    }
};

class Solution1 {
  public:
    void flatten(TreeNode *root) {
        for (; root; root = root->right) {
            if (root->left) {
                TreeNode *x = root->left;
                while (x->right)
                    x = x->right;
                x->right = root->right;
                root->right = root->left;
                root->left = nullptr;
            }
        }
    }
};

TEST_CASE {
    Solution s;

    TreeNode c1(1), c2(2), c3(3), c4(4), c5(5), c6(6), *r = &c1;
    c1.left = &c2;
    c1.right = &c5;
    c2.left = &c3;
    c2.right = &c4;
    c5.right = &c6;

    s.flatten(&c1);
    while (r) {
        printf("%d->", r->val);
        r = r->right;
    }

    printf("NULL\n");
}

int main(int argc, char *argv[]) {
    TEST(Solution);
    TEST(Solution1);

    return 0;
}
