#include "common.h"

// Longest Substring Without Repeating Characters
class Solution {
  public:
    int lengthOfLongestSubstring(string s) {
        vector<int> visited(127, -1);
        int r = 0, m = -1;
        for (int i = 0; i < s.size(); i++) {
            m = max(m, visited[s[i]]);
            r = max(r, i - m);
            visited[s[i]] = i;
        }

        return r;
    }
};

class Solution1 {
  public:
    int lengthOfLongestSubstring(string s) {
        vector<bool> c(127);
        int r = 0;
        for (int i = 0, j = 0; j < s.size(); i++) {
            for (; j < s.size() && !c[s[j]]; j++)
                c[s[j]] = true;
            r = max(r, j - i);
            // Let j move forward
            c[s[i]] = false;
        }
        return r;
    }
};

TEST_CASE {
    Solution s;

    assert(s.lengthOfLongestSubstring("aaa") == 1);
    assert(s.lengthOfLongestSubstring("aba") == 2);
    assert(s.lengthOfLongestSubstring("abc") == 3);
    assert(s.lengthOfLongestSubstring("characters") == 6);
}

int main(int argc, char *argv[]) {
    TEST(Solution);
    TEST(Solution1);

    return 0;
}
