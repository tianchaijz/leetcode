#include "common.h"

class Solution {
  public:
    string convertToTitle(int n) {
        string title = "";
        while (n > 0) {
            char c = (n - 1) % 26;
            title += string{static_cast<char>(c + 'A')};
            n = (n - c) / 26;
        }

        std::reverse(title.begin(), title.end());

        return title;
    }
};

TEST_CASE {
    Solution s;

    assert(s.convertToTitle(1) == "A");
    assert(s.convertToTitle(2) == "B");
    assert(s.convertToTitle(26) == "Z");
    assert(s.convertToTitle(27) == "AA");
    assert(s.convertToTitle(28) == "AB");
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
