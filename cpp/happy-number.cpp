#include "common.h"

class Solution {
  public:
    bool isHappy(int n) {
        int sum = 0;
        unordered_set<int> mem;
        while (mem.insert(n).second && n > 0 && n != 1) {
            for (; n; n /= 10) {
                sum += (n % 10) * (n % 10);
            }
            n = sum;
            sum = 0;
        }

        return n == 1;
    }
};

TEST_CASE {
    Solution s;

    assert(s.isHappy(19) == true);
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
