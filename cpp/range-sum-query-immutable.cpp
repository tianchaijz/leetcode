#include "common.h"

class NumArray {
  public:
    NumArray(vector<int> &nums) {
        accum_ = nums;
        for (size_t i = 1; i < nums.size(); ++i) {
            accum_[i] += accum_[i - 1];
        }
    }

    int sumRange(int i, int j) {
        return accum_[j] - (i ? accum_[i - 1] : 0);
    }

  private:
    vector<int> accum_;
};

TEST_CASE {
}

int main(int argc, char *argv[]) {
    TEST(NumArray);

    return 0;
}
