#include "common.h"

class Solution {
  public:
    int strStr(string haystack, string needle) {
        return haystack.find(needle, 0);
    }
};

class Solution1 {
  public:
    char *strStr(char *haystack, char *needle) {
        char c = *needle++;
        if (!c)
            return haystack;

        size_t len = strlen(needle);
        do {
            char sc;
            do {
                sc = *haystack++;
                if (!sc)
                    return NULL;
            } while (sc != c);
        } while (strncmp(haystack, needle, len) != 0);

        return haystack[-1];
    }
};

TEST_CASE {
    Solution s;
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
