#include "common.h"

// https://github.com/MaskRay/LeetCode/blob/master/validate-ip-address.cc
class Solution {
private:
  bool IPv4(string ip) {
    size_t idx = 0, ip_len = ip.size();
    for (size_t j, i = 0; i < ip_len; i = j + 1) {
      int byte = 0;
      for (j = i; j < ip_len && isdigit(ip[j]); ++j) {
        byte = byte * 10 + ip[j] - '0';
      };

      if (j == i || j - i > 3 || (j - i > 1 && ip[i] == '0') ||
          (j < ip_len && ip[j] != '.')) {
        return false;
      }

      if (byte > 255) {
        return false;
      }

      ++idx;
    }

    return idx == 4 && ip_len && ip.back() != '.';
  }

  bool IPv6(string ip) {
    size_t idx = 0, ip_len = ip.size();
    for (size_t j, i = 0; i < ip_len; i = j + 1) {
      for (j = i; j < ip_len && isxdigit(ip[j]); ++j)
        ;
      if (j == i || j - i > 4 || (j < ip_len && ip[j] != ':')) {
        return false;
      }
      ++idx;
    }

    return idx == 8 && ip_len && ip.back() != ':';
  }

public:
  string validIPAddress(string IP) {
    if (IPv4(IP))
      return "IPv4";
    if (IPv6(IP))
      return "IPv6";
    return "Neither";
  }
};

TEST_CASE {
  Solution s;

  assert(s.validIPAddress(".") == "Neither");
  assert(s.validIPAddress("..") == "Neither");
  assert(s.validIPAddress("...") == "Neither");
  assert(s.validIPAddress("....") == "Neither");
  assert(s.validIPAddress("...0") == "Neither");
  assert(s.validIPAddress("..0.0") == "Neither");
  assert(s.validIPAddress(".0.0.0") == "Neither");
  assert(s.validIPAddress("172.16.1.1.") == "Neither");
  assert(s.validIPAddress(".172.16.1.1") == "Neither");
  assert(s.validIPAddress("172.016.1.1") == "Neither");
  assert(s.validIPAddress("256.256.256.256") == "Neither");
  assert(s.validIPAddress("02001:0db8:85a3:0000:0000:8a2e:0370:7334") ==
         "Neither");

  assert(s.validIPAddress("0.0.0.0") == "IPv4");
  assert(s.validIPAddress("172.16.1.1") == "IPv4");
  assert(s.validIPAddress("2001:0db8:85a3:0000:0000:8a2e:0370:7334") == "IPv6");
}

int main(int argc, char *argv[]) {
  TEST(Solution);

  return 0;
}
