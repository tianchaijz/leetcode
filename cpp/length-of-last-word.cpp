#include "common.h"

class Solution {
  public:
    int lengthOfLastWord(string s) {
        std::istringstream iss(s);
        string w;
        while (iss >> w)
            ;
        return w.size();
    }
};

class Solution1 {
  public:
    int lengthOfLastWord(string s) {
        int len = 0, tail = s.size() - 1;
        while (tail >= 0 && s[tail] == ' ')
            --tail;
        while (tail >= 0 && s[tail] != ' ')
            --tail, ++len;
        return len;
    }
};

class Solution2 {
  public:
    int lengthOfLastWord(string s) {
        auto it = s.begin();
        int len = 0;
        while (it != s.end()) {
            if (*it++ != ' ') // ++ fallthrough
                ++len;
            else if (it != s.end() && *it != ' ')
                len = 0;
        }

        return len;
    }
};

TEST_CASE {
    Solution s;

    assert(s.lengthOfLastWord("Hello") == 5);
    assert(s.lengthOfLastWord("Hello ") == 5);
    assert(s.lengthOfLastWord("Hello World") == 5);
}

int main(int argc, char *argv[]) {
    TEST(Solution);
    TEST(Solution1);
    TEST(Solution2);

    return 0;
}
