#include "common.h"

class Solution {
  public:
    int minMoves2(vector<int> &nums) {
        if (nums.empty())
            return 0;
        nth_element(nums.begin(), nums.begin() + nums.size() / 2, nums.end());
        int r = 0, mid = nums[nums.size() / 2];
        for (int n : nums)
            r += abs(n - mid);
        return r;
    }
};

class Solution1 {
  public:
    int minMoves2(vector<int> &nums) {
        int r = 0, mid = findKth(nums, nums.size() / 2, 0, nums.size() - 1);
        for (int n : nums)
            r += abs(n - mid);
        return r;
    }

  private:
    int findKth(vector<int> &nums, int k, int lo, int hi) {
        int pivot = nums[hi], left = lo, right = hi;
        while (left < right) {
            while (left < right && nums[left] < pivot)
                ++left;
            while (left < right && nums[right] >= pivot)
                --right;
            if (left == right)
                break;
            swap(nums[left], nums[right]);
        }

        swap(nums[left], nums[hi]);

        if (k == left)
            return pivot;
        else if (k < left)
            return findKth(nums, k, lo, left - 1);
        else
            return findKth(nums, k, left + 1, hi);
    };
};

TEST_CASE {
    Solution s;

    vector<int> v1{1, 2, 3};

    assert(s.minMoves2(v1) == 2);
}

int main(int argc, char *argv[]) {
    TEST(Solution);
    TEST(Solution1);

    return 0;
}
