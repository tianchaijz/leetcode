#include "common.h"

class Solution {
  public:
    vector<int> plusOne(vector<int> &digits) {
        int carry = 1;
        for (int i = digits.size() - 1; i >= 0; --i) {
            carry += digits[i];
            digits[i] = carry % 10;
            carry /= 10;
        }

        if (carry)
            digits.insert(digits.begin(), 1);

        return digits;
    }
};

class Solution1 {
  public:
    vector<int> plusOne(vector<int> &digits) {
        int carry = 1;
        for (auto it = digits.rbegin(); it != digits.rend(); ++it) {
            carry += *it;
            *it = carry % 10;
            carry /= 10;
        }

        if (carry)
            digits.insert(digits.begin(), 1);

        return digits;
    }
};

class Solution2 {
  public:
    vector<int> plusOne(vector<int> &digits) {
        int carry = 1;
        for_each(digits.rbegin(), digits.rend(), [&carry](int &n) {
            carry += n;
            n = carry % 10;
            carry /= 10;
        });

        if (carry)
            digits.insert(digits.begin(), 1);

        return digits;
    }
};

class Solution3 {
  public:
    vector<int> plusOne(vector<int> &digits) {
        for (int i = digits.size(); i--; digits[i] = 0)
            if (digits[i]++ < 9)
                return digits;
        digits[0]++;
        digits.push_back(0);
        return digits;
    }
};

TEST_CASE {
    Solution s;
    vector<int> v1 = {1};
    vector<int> v2 = {9};
    vector<int> v3 = {1, 1};

    assert(s.plusOne(v1) == (vector<int>{2}));
    assert(s.plusOne(v2) == (vector<int>{1, 0}));
    assert(s.plusOne(v3) == (vector<int>{1, 2}));
}

int main(int argc, char *argv[]) {
    TEST(Solution);
    TEST(Solution1);
    TEST(Solution2);
    TEST(Solution3);

    return 0;
}
