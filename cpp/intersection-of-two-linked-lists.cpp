#include "common.h"

class Solution {
  public:
    ListNode *getIntersectionNode(ListNode *headA, ListNode *headB) {
        ListNode *pa = headA, *pb = headB;
        while (pa != pb) {
            pa = pa ? pa->next : headB;
            pb = pb ? pb->next : headA;
        }
        return pa;
    }
};

class Solution1 {
  public:
    ListNode *getIntersectionNode(ListNode *headA, ListNode *headB) {
        int d = 0;
        for (ListNode *p = headA; p; p = p->next)
            ++d;
        for (ListNode *p = headB; p; p = p->next)
            --d;
        while (d > 0) {
            headA = headA->next;
            --d;
        }
        while (d < 0) {
            headB = headB->next;
            ++d;
        }

        while (headA != headB) {
            headA = headA->next;
            headB = headB->next;
        }
        return headA;
    }
};

TEST_CASE {
    Solution s;
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
