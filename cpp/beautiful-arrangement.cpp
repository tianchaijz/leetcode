#include "common.h"

class Solution {
  public:
    int countArrangement(int N) {
        vector<int> v(N + 1);
        for (int i = 1; i <= N; ++i)
            v[i] = i;
        return count(N, v);
    }

  private:
    int count(int n, vector<int> &v) {
        if (n < 2)
            return 1;
        int cnt = 0;
        for (int i = 1; i <= n; ++i) {
            if (v[i] % n == 0 || n % v[i] == 0) {
                swap(v[i], v[n]);
                cnt += count(n - 1, v);
                swap(v[i], v[n]);
            }
        }

        return cnt;
    }
};

class Solution1 {
  public:
    int countArrangement(int N) {
        used.resize(N + 1);
        count = 0;
        std::fill(used.begin(), used.end(), false);
        dfs(N, N);

        return count;
    }

  private:
    vector<bool> used;
    int count = 0;
    void dfs(int N, int k) {
        if (k < 2) {
            ++count;
            return;
        }

        // just try every possible number(k) at each position
        for (int i = 1; i <= N; ++i) {
            if (used[i] || (k % i && i % k))
                continue;
            used[i] = true;
            dfs(N, k - 1);
            used[i] = false;
        }
    }
};

TEST_CASE {
    Solution s;

    assert(s.countArrangement(1) == 1);
    assert(s.countArrangement(2) == 2);
}

int main(int argc, char *argv[]) {
    TEST(Solution);
    TEST(Solution1);

    return 0;
}
