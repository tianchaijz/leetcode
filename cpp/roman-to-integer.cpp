#include "common.h"

class Solution {
  public:
    int romanToInt(string s) {
        if (s.empty())
            return 0;

        int roman[26] = {};
        roman['I' - 'A'] = 1;
        roman['V' - 'A'] = 5;
        roman['X' - 'A'] = 10;
        roman['L' - 'A'] = 50;
        roman['C' - 'A'] = 100;
        roman['D' - 'A'] = 500;
        roman['M' - 'A'] = 1000;

        int l = roman[s[0] - 'A'], sum = 0;
        for (int i = 1; i < s.size(); ++i) {
            int n = roman[s[i] - 'A'];
            if (n > l)
                sum -= l;
            else
                sum += l;
            l = n;
        }

        return sum + l;
    }
};

TEST_CASE {
    Solution s;

    assert(s.romanToInt("DCXXI") == 621);
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
