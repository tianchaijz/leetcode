#include "common.h"

class Solution {
  public:
    bool canWinNim(int n) {
        return n % 4;
    }
};

class Solution1 {
  public:
    bool canWinNim(int n) {
        bool a = 1, b = 1, c = 1, can = 1;
        while (n-- > 3) {
            can = !(a && b && c);
            a = b;
            b = c;
            c = can;
        }

        return can;
    }
};

TEST_CASE {
    Solution s;

    assert(s.canWinNim(1) == true);
    assert(s.canWinNim(2) == true);
    assert(s.canWinNim(3) == true);
    assert(s.canWinNim(4) == false);
    assert(s.canWinNim(5) == true);
}

int main(int argc, char *argv[]) {
    TEST(Solution);
    TEST(Solution1);

    return 0;
}
