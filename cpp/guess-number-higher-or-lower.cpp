#include "common.h"

static int number = 6;

// Forward declaration of guess API.
// @param num, your guess
// @return -1 if my number is lower, 1 if my number is higher, otherwise return 0
int guess(int num) {
    if (number == num)
        return 0;
    else if (number > num)
        return 1;
    else
        return -1;
}

class Solution {
  public:
    int guessNumber(int n) {
        int lo = 1, hi = n;
        while (lo < hi) {
            int mid = lo + (hi - lo) / 2;
            int t = guess(mid);
            if (t == 0)
                return mid;
            else if (t > 0)
                lo = mid + 1;
            else
                hi = mid - 1;
        }

        return lo;
    }
};

TEST_CASE {
    Solution s;

    for (int i = 1; i < 100; ++i) {
        number = i;
        assert(s.guessNumber(i) == i);
    }
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
