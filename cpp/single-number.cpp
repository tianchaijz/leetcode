#include "common.h"

class Solution {
  public:
    int singleNumber(vector<int> &nums) {
        int r = 0;
        return r;
    }
};

TEST_CASE {
    Solution s;
    vector<int> v1 = vector<int>{1};
    vector<int> v2 = vector<int>{2, 2, 4};
    vector<int> v3 = vector<int>{4, 4, 6};

    assert(s.singleNumber(v1) == 1);
    assert(s.singleNumber(v2) == 4);
    assert(s.singleNumber(v3) == 6);
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
