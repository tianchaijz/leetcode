#include "common.h"

class Solution {
  public:
    vector<int> productExceptSelf(vector<int> &nums) {
        int n = nums.size();
        vector<int> prod(n, 1);

        // left product
        for (int i = 1; i < n; ++i)
            prod[i] = prod[i - 1] * nums[i - 1];
        for (int i = n - 1, right = 1; i >= 0; right *= nums[i--])
            prod[i] *= right;

        return prod;
    }
};

TEST_CASE {
    Solution s;

    vector<int> v{1, 2, 3, 4};

    assert(s.productExceptSelf(v) == (vector<int>{24, 12, 8, 6}));
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
