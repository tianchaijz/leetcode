#include "common.h"

class Solution {
  public:
    int hammingWeight(uint32_t n) {
        return bitset<8 * sizeof(uint32_t)>(n).count();
    }
};

class Solution1 {
  public:
    int hammingWeight(uint32_t n) {
        int c = 0;
        for (; n; ++c)
            n &= n - 1; // delete one '1' from n
        return c;
    }
};

TEST_CASE {
    Solution s;

    assert(s.hammingWeight(3) == 2);
    assert(s.hammingWeight(11) == 3);
}

int main(int argc, char *argv[]) {
    TEST(Solution);
    TEST(Solution1);

    return 0;
}
