#include "common.h"

// Median of Two Sorted Arrays
class Solution {
  public:
    double findMedianSortedArrays(vector<int> &nums1, vector<int> &nums2) {
        int size = nums1.size() + nums2.size();

        vector<int> nums;
        nums.reserve(size);

        nums.insert(nums.end(), nums1.begin(), nums1.end());
        nums.insert(nums.end(), nums2.begin(), nums2.end());

        sort(nums.begin(), nums.end());

        if (size & 1)
            return nums[size / 2];
        return (nums[(size - 1) / 2] + nums[size / 2]) / 2.0;
    }
};

class Solution1 {
  public:
    double findMedianSortedArrays(vector<int> &a, vector<int> &b) {
        int l = a.size() + b.size();
        if (l & 1)
            return findKth(a, 0, b, 0, l / 2 + 1);
        return (findKth(a, 0, b, 0, l / 2) + findKth(a, 0, b, 0, l / 2 + 1)) / 2.0;
    }

  private:
    int findKth(vector<int> &a, int m, vector<int> &b, int n, int k) {
        if (m >= a.size())
            return b[n + k - 1];
        if (n >= b.size())
            return a[m + k - 1];
        if (k == 1)
            return min(a[m], b[n]);

        // n = e - s + 1; e = n - 1 + s; n = k / 2; p = n - 1 = k / 2 - 1
        int p = k / 2 - 1, i = m + p, j = n + p;
        // 小的一半会被略过, kth 肯定不在这一半里
        if (j >= b.size() || (i < a.size() && a[i] < b[j]))
            return findKth(a, i + 1, b, n, k - k / 2);
        return findKth(a, m, b, j + 1, k - k / 2);
    }
};

class Solution2 {
  public:
    double findMedianSortedArrays(vector<int> &a, vector<int> &b) {
        int l = a.size() + b.size();
        if (l & 1)
            return findKth(a, b, l / 2 + 1);
        return (findKth(a, b, l / 2) + findKth(a, b, l / 2 + 1)) / 2.0;
    }

  private:
    int findKth(vector<int> &a, vector<int> &b, int k) {
        int m = a.size(), n = b.size(), i = 0, j = 0;
        while (k > 1) {
            int p = k / 2 - 1;
            if (j + p >= n || (i + p < m && a[i + p] < b[j + p]))
                i += p + 1;
            else
                j += p + 1;
            k -= p + 1;
        }

        return j >= n || (i < m && a[i] < b[j]) ? a[i] : b[j];
    }
};

class Solution3 {
  public:
    double findMedianSortedArrays(vector<int> &a, vector<int> &b) {
        int m = a.size(), n = b.size(), i = 0, j = 0, k = (m + n - 1) >> 1;
        // find kth element
        while (k > 0) {
            int p = (k - 1) >> 1;
            if (j + p >= n || (i + p < m && a[i + p] < b[j + p]))
                i += p + 1;
            else
                j += p + 1;
            k -= p + 1;
        }

        // 最后看 i, j 对应的元素哪个排在前面
        int s = j >= n || (i < m && a[i] < b[j]) ? a[i++] : b[j++];
        return m + n & 1 ? s : (j >= n || (i < m && a[i] < b[j]) ? s + a[i] : s + b[j]) / 2.0;
    }
};

class Solution4 {
  public:
    double findMedianSortedArrays(vector<int> &a, vector<int> &b) {
        int m = a.size(), n = b.size(), i = 0, j = 0, k = (m + n + 1) / 2;
        while (k > 1) {
            int p = k / 2 - 1;
            if (j + p >= n || (i + p < m && a[i + p] < b[j + p]))
                i += p + 1;
            else
                j += p + 1;
            k -= p + 1;
        }

        int s = j >= n || (i < m && a[i] < b[j]) ? a[i++] : b[j++];
        return m + n & 1 ? s : (j >= n || (i < m && a[i] < b[j]) ? s + a[i] : s + b[j]) / 2.0;
    }
};

TEST_CASE {
    Solution s;

    vector<int> v1 = {1, 2};
    vector<int> v2 = {3, 4};
    vector<int> v3 = {1, 3};
    vector<int> v4 = {2};
    vector<int> v5 = {1};
    vector<int> v6 = {2, 3, 4, 5, 6};

    assert(s.findMedianSortedArrays(v1, v2) == 2.5);
    assert(s.findMedianSortedArrays(v3, v4) == 2);
    assert(s.findMedianSortedArrays(v5, v6) == 3.5);
}

int main(int argc, char *argv[]) {
    TEST(Solution);
    TEST(Solution1);
    TEST(Solution2);
    TEST(Solution3);
    TEST(Solution4);

    return 0;
}
