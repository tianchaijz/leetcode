#define TEST_CALLS 1
#include "common.h"

class Solution {
  public:
    vector<vector<int>> levelOrder(TreeNode *root) {
        vector<vector<int>> r;
        if (root == NULL)
            return r;

        queue<TreeNode *> q;
        q.push(root);
        while (!q.empty()) {
            vector<int> v;
            for (int i = 0, j = q.size(); i < j; ++i) {
                TreeNode *node = q.front();
                q.pop();
                v.push_back(node->val);
                if (node->left)
                    q.push(node->left);
                if (node->right)
                    q.push(node->right);
            }
            r.push_back(v);
        }

        return r;
    }
};

class Solution1 {
  public:
    vector<vector<int>> levelOrder(TreeNode *root) {
        vector<vector<int>> r;
        if (root == NULL)
            return r;

        stack<pair<TreeNode *, int>> s;
        s.push(make_pair(root, 0));
        while (!s.empty()) {
            auto p = s.top();
            s.pop();
            if (r.size() <= p.second)
                r.emplace_back();
            r[p.second].push_back(p.first->val);
            if (p.first->right)
                s.push(make_pair(p.first->right, p.second + 1));
            if (p.first->left)
                s.push(make_pair(p.first->left, p.second + 1));
        }

        return r;
    }
};

TEST_CASE {
    Solution s;

    TreeNode a{0};
    TreeNode b{1};
    TreeNode c{3};
    TreeNode d{4};
    TreeNode e{5, &a, &b};
    TreeNode f{6, &c, &d};
    TreeNode g{7, &e, &f};

    auto r = s.levelOrder(&g);
    for (auto v : r) {
        std::cout << r << std::endl;
    }
}

int main(int argc, char *argv[]) {
    TEST(Solution);
    TEST(Solution1);

    return 0;
}
