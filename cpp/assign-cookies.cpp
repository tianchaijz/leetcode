#include "common.h"

class Solution {
  public:
    int findContentChildren(vector<int> &g, vector<int> &s) {
        sort(g.begin(), g.end());
        sort(s.begin(), s.end());

        int i = 0;
        for (int j = 0; i < g.size() && j < s.size();) {
            if (s[j] >= g[i]) {
                ++i;
                ++j;
            } else {
                ++j;
            }
        }
        return i;
    }
};

class Solution1 {
  public:
    int findContentChildren(vector<int> &g, vector<int> &s) {
        sort(g.begin(), g.end());
        sort(s.begin(), s.end());

        auto i = s.begin();
        int r = 0;
        for (int x : g) {
            while (i != s.end() && *i < x)
                ++i;
            if (i == s.end())
                break;

            ++i;
            ++r;
        }

        return r;
    };
};

TEST_CASE {
    Solution s;

    vector<int> g1{1, 2, 3};
    vector<int> s1{1, 1};

    vector<int> g2{1, 2};
    vector<int> s2{1, 2, 3};

    vector<int> g3{10, 9, 8, 7};
    vector<int> s3{5, 6, 7, 8};

    assert(s.findContentChildren(g1, s1) == 1);
    assert(s.findContentChildren(g2, s2) == 2);
    assert(s.findContentChildren(g3, s3) == 2);
}

int main(int argc, char *argv[]) {
    TEST(Solution);
    TEST(Solution1);

    return 0;
}
