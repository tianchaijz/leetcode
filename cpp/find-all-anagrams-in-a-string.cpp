#include "common.h"

class Solution {
  public:
    vector<int> findAnagrams(string s, string p) {
        vector<int> r;
        if (s.size() == 0 || p.size() == 0)
            return r;

        int c[256] = {0};
        for (auto &a : p)
            c[a - '\0']++;

        size_t lo = 0, hi = 0, count = p.size();
        // sliding window
        while (hi < s.size()) {
            if (c[s[hi++] - '\0']-- > 0)
                --count;
            if (count == 0)
                r.push_back(lo);
            // now window size equals p.size(), we advance lo
            if (hi - lo == p.size() && ++c[s[lo++] - '\0'] > 0)
                ++count;
        }

        return r;
    }
};

TEST_CASE {
    Solution s;

    assert(s.findAnagrams("abab", "ab") == (vector<int>{0, 1, 2}));
    assert(s.findAnagrams("aaaa", "a") == (vector<int>{0, 1, 2, 3}));
    assert(s.findAnagrams("cbaebabacd", "abc") == (vector<int>{0, 6}));
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
