#include "common.h"

class Solution {
  public:
    int removeElement(vector<int> &nums, int val) {
        int index = 0;
        for (int i = 0; i < nums.size(); ++i) {
            if (nums[i] != val) {
                nums[index++] = nums[i];
            }
        }

        return index;
    }
};

TEST_CASE {
    Solution s;
    vector<int> v1{3, 2, 2, 3};
    vector<int> v2{2, 2, 2, 2};

    assert(s.removeElement(v1, 3) == 2);
    assert(s.removeElement(v2, 2) == 0);
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
