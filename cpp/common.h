#ifndef COMMON_H
#define COMMON_H

#include <stdarg.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <cassert>
#include <cctype>
#include <cmath>
#include <ctime>
#include <iostream>
#include <numeric>
#include <sstream>
#include <utility>

#include <bitset>
#include <map>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

using std::map;
using std::pair;
using std::queue;
using std::set;
using std::priority_queue;
using std::stack;
using std::bitset;
using std::unordered_map;
using std::unordered_set;
using std::vector;
using std::string;

using std::swap;
using std::iota;
using std::sort;
using std::max;
using std::min;
using std::to_string;
using std::make_pair;

#ifdef assert
#undef assert
#endif

#define assert(expr)                                                         \
    if (!(expr)) {                                                           \
        fprintf(stderr, "%s:%d assertion failure: %s\n", __FILE__, __LINE__, \
                #expr);                                                      \
        abort();                                                             \
    }

template <typename T>
std::ostream &operator<<(std::ostream &os, const std::vector<T> &vec) {
    for (auto it = vec.begin(); it != vec.end(); ++it) {
        os << *it << " ";
    }

    return os;
}

class timer {
  public:
    timer() : start_(0) {
    }
    timer(const timer &) = delete;
    void operator=(const timer &) = delete;

    void start() {
        start_ = clock();
    }

    double elapsed_milliseconds() const {
        return static_cast<double>(clock() - start_) / CLOCKS_PER_SEC * 1000;
    }

  private:
    clock_t start_;
};

#define TEST_CASE                \
    template <typename Solution> \
    void T()

#ifndef TEST_CALLS
#define TEST_CALLS 1000
#endif

#define TEST(Solution)                                                        \
    do {                                                                      \
        timer t;                                                              \
        t.start();                                                            \
        for (int i = 0; i < TEST_CALLS; ++i) {                                \
            T<Solution>();                                                    \
        }                                                                     \
        double elapsed = t.elapsed_milliseconds();                            \
        printf("[%s](%d calls): %.2f milliseconds.\n", #Solution, TEST_CALLS, \
               elapsed);                                                      \
    } while (0)

#ifndef TREE_NODE
struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL) {
    }
    TreeNode(int x, TreeNode *left, TreeNode *right)
        : val(x), left(left), right(right) {
    }
};
#endif

#ifndef LIST_NODE
struct ListNode {
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {
    }
    ListNode(int x, ListNode *next) : val(x), next(next) {
    }
};

void printList(ListNode *head) {
    while (head) {
        printf("%d->", head->val);
        head = head->next;
    }

    printf("NULL\n");
}

void printList(string msg, ListNode *head) {
    printf("%s", msg.c_str());
    printList(head);
}

template <typename T>
void printBytes(T &&x, size_t size = sizeof(T)) {
    unsigned char *p = reinterpret_cast<unsigned char *>(&x);
    for (size_t i = 0; i < size; ++i) {
        printf("%02hhX ", p[i]);
    }
    printf("\n");
}

ListNode *createList(int n) {
    ListNode *head = nullptr;
    for (int i = 0; i < n; ++i) {
        ListNode *node = new ListNode(i, head);
        head = node;
    }

    return head;
}

void freeList(ListNode *head) {
    ListNode *t;
    while (head) {
        t = head->next;
        delete head;
        head = t;
    }
}

#endif

#endif // COMMON_H
