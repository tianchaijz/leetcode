#define TEST_CALLS 1
#include "common.h"

class Solution {
  public:
    TreeNode *bstToDll(TreeNode *root) {
        if (root == nullptr)
            return nullptr;
        bstToDll(root->left);
        if (head_ == nullptr)
            head_ = root;
        else {
            tail_->right = root;
            root->left = tail_;
        }
        tail_ = root;
        bstToDll(root->right);

        return head_;
    }

  private:
    TreeNode *head_ = nullptr, *tail_ = nullptr;
};

TEST_CASE {
    Solution s;

    TreeNode c1(1), c2(2), c3(3), c4(4), c5(5), c6(6), *r = &c4;
    c4.left = &c2;
    c4.right = &c5;
    c2.left = &c1;
    c2.right = &c3;
    c5.right = &c6;

    TreeNode *head = s.bstToDll(r);
    for (; head; head = head->right) {
        printf("%d->", head->val);
        if (head->right == NULL)
            break;
    }
    printf("NULL\n");
    for (; head; head = head->left) {
        printf("%d->", head->val);
    }
    printf("NULL\n");
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
