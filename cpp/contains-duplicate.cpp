#include "common.h"

class Solution {
  public:
    bool containsDuplicate(vector<int> &nums) {
        return nums.size() > unordered_set<int>(nums.begin(), nums.end()).size();
    }
};

class Solution1 {
  public:
    bool containsDuplicate(vector<int> &nums) {
        unordered_set<int> s;
        for (int n : nums) {
            if (s.find(n) != s.end())
                return true;
            s.insert(n);
        }
        return false;
    }
};

TEST_CASE {
    Solution s;
    vector<int> v1{1};
    vector<int> v2{1, 2, 3};
    vector<int> v3{1, 2, 2};

    assert(s.containsDuplicate(v1) == false);
    assert(s.containsDuplicate(v2) == false);
    assert(s.containsDuplicate(v3) == true);
}

int main(int argc, char *argv[]) {
    TEST(Solution);
    TEST(Solution1);

    return 0;
}
