#include "common.h"

class MinStack {
  public:
    /** initialize your data structure here. */
    MinStack() {
    }

    void push(int x) {
        stack_.push(x);
        if (min_.empty() || x <= min_.top())
            min_.push(x);
    }

    void pop() {
        if (stack_.top() == min_.top())
            min_.pop();
        stack_.pop();
    }

    int top() {
        return stack_.top();
    }

    int getMin() {
        return min_.top();
    }

  private:
    stack<int> stack_, min_;
};

TEST_CASE {
    Solution s;
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
