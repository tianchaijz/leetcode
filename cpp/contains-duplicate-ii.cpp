#include "common.h"

class Solution {
  public:
    bool containsNearbyDuplicate(vector<int> &nums, int k) {
        vector<int> idx(nums.size());
        iota(idx.begin(), idx.end(), 0);
        sort(idx.begin(), idx.end(), [&nums](int x, int y) { return nums[x] < nums[y]; });

        for (size_t i = 1; i < idx.size(); ++i) {
            for (; i < idx.size() && nums[idx[i - 1]] != nums[idx[i]]; ++i)
                ;
            if (i < idx.size() && abs(idx[i] - idx[i - 1]) <= k)
                return true;
        }

        return false;
    }
};

class Solution1 {
  public:
    bool containsNearbyDuplicate(vector<int> &nums, int k) {
        if (k < 1)
            return false;
        unordered_set<int> s;
        for (size_t i = 0; i < nums.size(); ++i) {
            if (i > static_cast<size_t>(k))
                s.erase(nums[i - k - 1]);
            if (s.find(nums[i]) != s.end())
                return true;
            s.insert(nums[i]);
        }

        return false;
    }
};

class Solution2 {
  public:
    bool containsNearbyDuplicate(vector<int> &nums, int k) {
        map<int, int> m;
        for (size_t i = 0; i < nums.size(); ++i) {
            if (m.count(nums[i]) && i - m[nums[i]] <= k)
                return true;
            m[nums[i]] = i;
        }
        return false;
    }
};

TEST_CASE {
    Solution s;

    vector<int> v{1, 2, 1, 2};

    assert(s.containsNearbyDuplicate(v, 2));
    assert(s.containsNearbyDuplicate(v, 1) == false);
}

int main(int argc, char *argv[]) {
    TEST(Solution);
    TEST(Solution1);
    TEST(Solution2);

    return 0;
}
