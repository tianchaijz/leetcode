#include "common.h"

class Solution {
    int aux(TreeNode *r, bool left) {
        return r ? r->left || r->right ? aux(r->left, 1) + aux(r->right, 0) : left ? r->val : 0 : 0;
    }

  public:
    int sumOfLeftLeaves(TreeNode *root) {
        return aux(root, 0);
    }
};

class Solution1 {
  public:
    int sumOfLeftLeaves(TreeNode *root) {
        return aux(root, 0);
    }

  private:
    int aux(TreeNode *r, bool left) {
        if (r == NULL)
            return 0;

        if (r->left || r->right)
            return aux(r->left, 1) + aux(r->right, 0);
        else if (left)
            return r->val;

        return 0;
    }
};

TEST_CASE {
    Solution s;
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
