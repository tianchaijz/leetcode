#include "common.h"

class Stack {
  public:
    void push(int x) {
        int size = queue_.size();
        queue_.push(x);
        while (size--) {
            queue_.push(queue_.front());
            queue_.pop();
        }
    }

    void pop() {
        queue_.pop();
    }

    int top() {
        return queue_.front();
    }

    bool empty() {
        return queue_.empty();
    }

  private:
    queue<int> queue_;
};

TEST_CASE {
    Solution s;
}

int main(int argc, char *argv[]) {
    TEST(Stack);

    return 0;
}
