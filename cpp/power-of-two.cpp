#include "common.h"

class Solution {
  public:
    bool isPowerOfTwo(int n) {
        while (n > 0 && n % 2 == 0)
            n >>= 1;
        return n == 1;
    }
};

class Solution1 {
  public:
    bool isPowerOfTwo(int n) {
        return n > 0 && !(n & (n - 1));
    }
};

TEST_CASE {
    Solution s;
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
