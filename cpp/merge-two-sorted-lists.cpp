#include "common.h"

class Solution {
  public:
    ListNode *mergeTwoLists(ListNode *x, ListNode *y) {
        ListNode dumy{0}, *p = &dumy;
        while (x && y) {
            if (x->val < y->val) {
                p->next = x;
                x = x->next;
            } else {
                p->next = y;
                y = y->next;
            }
            p = p->next;
        }
        p->next = x ? x : y;

        return dumy.next;
    }
};

class Solution1 {
  public:
    ListNode *mergeTwoLists(ListNode *x, ListNode *y) {
        if (x == NULL)
            return y;
        if (y == NULL)
            return x;
        if (x->val < y->val) {
            x->next = mergeTwoLists(x->next, y);
            return x;
        }
        y->next = mergeTwoLists(x, y->next);
        return y;
    }
};

class Solution2 {
  public:
    ListNode *mergeTwoLists(ListNode *x, ListNode *y) {
        if (x == NULL)
            return y;
        if (y == NULL)
            return x;

        ListNode *head, *p;
        if (x->val < y->val) {
            head = x;
            x = x->next;
        } else {
            head = y;
            y = y->next;
        }

        p = head;
        while (x && y) {
            if (x->val < y->val) {
                p->next = x;
                x = x->next;
            } else {
                p->next = y;
                y = y->next;
            }

            p = p->next;
        }

        p->next = x ? x : y;

        return head;
    }
};

int count = 0;
TEST_CASE {
    Solution s;

    ListNode a{2};
    ListNode b{1, &a};
    ListNode x{0, &b};
    ListNode y{1};

    ListNode *z = s.mergeTwoLists(&x, &y);
    if (count % TEST_CALLS == 0)
        printList(z);

    ++count;
}

int main(int argc, char *argv[]) {
    TEST(Solution);
    TEST(Solution1);
    TEST(Solution2);

    return 0;
}
