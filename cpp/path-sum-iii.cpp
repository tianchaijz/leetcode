#include "common.h"

class Solution {
  public:
    int pathSum(TreeNode *root, int sum) {
        if (!root)
            return 0;

        int paths = 0;
        vector<int> accum;

        dfs(root, sum, accum, paths);

        return paths;
    }

  private:
    void dfs(TreeNode *root, int sum, vector<int> accum, int &paths) {
        accum.push_back(root->val);
        int num = 0;
        for (int i = accum.size() - 1; i >= 0; --i) {
            num += accum[i];
            if (num == sum)
                ++paths;
        }

        if (root->left)
            dfs(root->left, sum, accum, paths);
        if (root->right)
            dfs(root->right, sum, accum, paths);
    }
};

// The sum from any node in the middle of the path to the current node ==
// the difference between the sum from the root to the current node and the prefix sum of the node in the middle.
// https://discuss.leetcode.com/topic/64526/17-ms-o-n-java-prefix-sum-method/15
class Solution1 {
  public:
    int pathSum(TreeNode *root, int sum) {
        this->sum = sum;
        m[0] = 1;
        aux(root, 0);
        return r;
    }

  private:
    int sum, r = 0;
    unordered_map<int, int> m;
    void aux(TreeNode *x, int accum) {
        if (!x)
            return;
        accum += x->val;
        r += m[accum - sum];
        m[accum]++;
        aux(x->left, accum);
        aux(x->right, accum);
        // restore the map, as the recursion goes from the bottom to the top
        m[accum]--;
    }
};

TEST_CASE {
    Solution s;
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
