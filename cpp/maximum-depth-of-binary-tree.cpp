#include "common.h"

class Solution {
  public:
    int maxDepth(TreeNode *root) {
        return root == NULL ? 0 : max(maxDepth(root->left), maxDepth(root->right)) + 1;
    }
};

class Solution1 {
  public:
    int maxDepth(TreeNode *root) {
        return bfs(root);
    }

  private:
    int bfs(TreeNode *root) {
        if (root == NULL)
            return 0;

        int depth = 0;
        queue<TreeNode *> q;

        q.push(root);

        while (!q.empty()) {
            ++depth;
            for (int i = 0, j = q.size(); i < j; ++i) {
                TreeNode *p = q.front();
                q.pop();

                if (p->left)
                    q.push(p->left);
                if (p->right)
                    q.push(p->right);
            }
        }

        return depth;
    }
};

TEST_CASE {
    Solution s;
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
