#define TEST_CALLS 1
#include "common.h"

class Solution {
  public:
    vector<string> binaryTreePaths(TreeNode *root) {
        vector<string> paths;
        dfs(root, paths);
        return paths;
    }

  private:
    vector<TreeNode *> trace_;
    void dfs(TreeNode *root, vector<string> &paths) {
        if (root == nullptr) {
            return;
        }

        trace_.push_back(root);

        if (root->left) {
            dfs(root->left, paths);
        }

        if (root->right) {
            dfs(root->right, paths);
        }

        // leaf node
        if (root->left == nullptr && root->right == nullptr) {
            string path;
            for (size_t i = 0; i < trace_.size(); ++i) {
                if (i < trace_.size() - 1)
                    path += to_string(trace_[i]->val) + "->";
                else
                    path += to_string(trace_[i]->val);
            }
            paths.push_back(path);
        }

        trace_.pop_back();
    }
};

class Solution1 {
  public:
    vector<string> binaryTreePaths(TreeNode *root) {
        aux(root, "");
        return paths;
    }

  private:
    vector<string> paths;
    void aux(TreeNode *root, string path) {
        if (root == nullptr)
            return;

        path = path.empty() ? to_string(root->val) : path + "->" + to_string(root->val);
        aux(root->left, path);
        aux(root->right, path);

        if (root->left == nullptr && root->right == nullptr)
            paths.push_back(path);
    }
};

TEST_CASE {
    Solution s;

    TreeNode a(5);
    TreeNode b(3);
    TreeNode c(2, &a, nullptr);
    TreeNode d(1, &c, &b);

    auto paths = s.binaryTreePaths(&d);
    for (auto &path : paths)
        std::cout << path << std::endl;
}

int main(int argc, char *argv[]) {
    TEST(Solution);
    TEST(Solution1);

    return 0;
}
