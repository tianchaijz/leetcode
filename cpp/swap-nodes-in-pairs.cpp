#include "common.h"

class Solution {
  public:
    ListNode *swapPairs(ListNode *head) {
        ListNode **p = &head;
        while (*p && (*p)->next) {
            ListNode *q = (*p)->next; // backup first node next pointer
            (*p)->next = q->next;     // update first node next pointer
            q->next = *p;             // update second node next pointer
            *p = q;                   // update joint pointer
            p = &(*p)->next->next;    // advance
        }

        return head;
    }
};

class Solution1 {
  public:
    ListNode *swapPairs(ListNode *head) {
        ListNode *h = head;
        while (head && head->next) {
            swap(head->val, head->next->val);
            head = head->next->next;
        }

        return h;
    }
};

TEST_CASE {
    Solution s;
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
