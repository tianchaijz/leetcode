#include "common.h"

class Solution {
  public:
    void deleteNode(ListNode *x) {
        auto y = x->next;
        swap(x->val, y->val);
        x->next = y->next;

        delete y;
    }
};

TEST_CASE {
    Solution s;
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
