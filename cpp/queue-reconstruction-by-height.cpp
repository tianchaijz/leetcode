#include "common.h"

class Solution {
  public:
    vector<pair<int, int>> reconstructQueue(vector<pair<int, int>> &people) {
        sort(people.begin(), people.end(),
             [](pair<int, int> a, pair<int, int> b) {
                 return a.first > b.first || (a.first == b.first && a.second < b.second);});

        vector<pair<int, int>> r;
        for (auto &p : people) {
            r.insert(r.begin() + p.second, p);
        }

        return r;
    }
};

TEST_CASE {
    Solution s;

    vector<pair<int, int>> people{{7, 0}, {4, 4}, {7, 1},
                                  {5, 0}, {6, 1}, {5, 2}},
        answer{{5, 0}, {7, 0}, {5, 2}, {6, 1}, {4, 4}, {7, 1}};

    assert(s.reconstructQueue(people) == answer);
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
