#include "common.h"

// For this, problem, for example, we have a = 1, b = 3,
// In bit representation, a = 0001, b = 0011,
// First, we can use "and"("&") operation between a and b to find a carry.
// carry = a & b, then carry = 0001
// Second, we can use "xor" ("^") operation between a and b to find the different bit, and assign it to a,
// Then, we shift carry one position left and assign it to b, b = 0010.
// Iterate until there is no carry (or b == 0)
class Solution {
  public:
    int getSum(int a, int b) {
        if (a == 0)
            return b;

        while (b) {
            int carry = a & b;
            a = a ^ b;
            b = carry << 1;
        }

        return a;
    }

    int getSubtract(int a, int b) {
        while (b) {
            int borrow = (~a) & b;
            a = a ^ b;
            b = borrow << 1;
        }

        return a;
    }
};

TEST_CASE {
    Solution s;

    assert(s.getSum(0, 0) == 0);
    assert(s.getSum(1, 0) == 1);
    assert(s.getSum(0, 1) == 1);
    assert(s.getSum(-1, 1) == 0);
    assert(s.getSum(1, -1) == 0);
    assert(s.getSum(1, 2) == 3);
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
