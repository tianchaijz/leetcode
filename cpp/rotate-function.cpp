#include "common.h"

class Solution {
  public:
    int maxRotateFunction(vector<int> &A) {
        if (A.empty())
            return 0;
        int sum = 0, rotation = 0;
        for (size_t i = 0; i < A.size(); ++i) {
            rotation += i * A[i];
            sum += A[i];
        }

        int r = rotation;
        // 错位相减递推
        for (size_t i = A.size() - 1; i > 0; --i) {
            rotation = rotation + sum - A[i] * A.size();
            r = max(r, rotation);
        }

        return r;
    }
};

TEST_CASE {
    Solution s;
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
