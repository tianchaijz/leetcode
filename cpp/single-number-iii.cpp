#include "common.h"

class Solution {
  public:
    vector<int> singleNumber(vector<int> &nums) {
        unordered_map<int, int> m;
        vector<int> r;

        for (int &n : nums)
            ++m[n];
        for (auto &x : m)
            if (x.second == 1)
                r.push_back(x.first);

        sort(r.begin(), r.end());

        return r;
    }
};

// https://discuss.leetcode.com/topic/21605/accepted-c-java-o-n-time-o-1-space-easy-solution-with-detail-explanations
class Solution1 {
  public:
    vector<int> singleNumber(vector<int> &nums) {
        vector<int> r(2, 0);
        int diff =
            std::accumulate(nums.begin(), nums.end(), 0, std::bit_xor<int>());

        // get its rightmost set bit
        diff &= -diff; // ~(diff - 1)
        for (int n : nums)
            r[!(n & diff)] ^= n;

        sort(r.begin(), r.end());

        return r;
    };
};

TEST_CASE {
    Solution s;

    vector<int> v{1, 2, 1, 3, 2, 5};

    assert(s.singleNumber(v) == (vector<int>{3, 5}));
}

int main(int argc, char *argv[]) {
    TEST(Solution);
    TEST(Solution1);

    printBytes(1);
    printBytes(-1);
    printBytes(-1 ^ 1);
    printBytes(1 & -1);

    return 0;
}
