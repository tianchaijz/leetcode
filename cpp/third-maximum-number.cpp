#include "common.h"

class Solution {
  public:
    int thirdMax(vector<int> &nums) {
        sort(nums.begin(), nums.end(), std::greater<int>());
        size_t i = 0, j = 0;
        for (; i < nums.size() - 1 && j < 2; ++i, ++j) {
            for (; i < nums.size() - 1 && nums[i] == nums[i + 1]; ++i)
                ;
        }

        return j == 2 && i < nums.size() ? nums[i] : nums.front();
    }
};

class Solution1 {
  public:
    int thirdMax(vector<int> &nums) {
        long a = LONG_MIN, b = LONG_MIN, c = LONG_MIN;
        for (auto &n : nums) {
            if (n > c && n != b && n != a) {
                c = n;
                if (c > b)
                    swap(c, b);
                if (b > a)
                    swap(b, a);
            }
        }

        return c == LONG_MIN ? a : c;
    }
};

class Solution2 {
  public:
    int thirdMax(vector<int> &nums) {
        set<int> top3;
        for (auto &n : nums) {
            top3.insert(n);
            if (top3.size() > 3)
                top3.erase(top3.begin());
        }

        return top3.size() == 3 ? *top3.begin() : *top3.rbegin();
    }
};

TEST_CASE {
    Solution s;

    vector<int> v1{1, 2};
    vector<int> v2{2, 2, 3, 1};
    vector<int> v3{1, 2, 3};
    vector<int> v4{1, 1, 2};
    vector<int> v5{1, 2, -2147483648};

    assert(s.thirdMax(v1) == 2);
    assert(s.thirdMax(v2) == 1);
    assert(s.thirdMax(v3) == 1);
    assert(s.thirdMax(v4) == 2);
    assert(s.thirdMax(v5) == -2147483648);
}

int main(int argc, char *argv[]) {
    TEST(Solution);
    TEST(Solution1);
    TEST(Solution2);

    return 0;
}
