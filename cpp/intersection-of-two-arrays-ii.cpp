#include "common.h"

class Solution {
  public:
    vector<int> intersect(vector<int> &a, vector<int> &b) {
        sort(a.begin(), a.end());
        sort(b.begin(), b.end());

        vector<int> r;
        for (int i = 0, j = 0; i < a.size() && j < b.size();) {
            if (a[i] == b[j]) {
                r.push_back(a[i]);
                ++i, ++j;
            } else if (a[i] > b[j])
                ++j;
            else
                ++i;
        }

        return r;
    }
};

TEST_CASE {
    Solution s;

    vector<int> v1{1, 2, 2, 1};
    vector<int> v2{2, 2};
    vector<int> v3{-2147483648, 1, 2, 3};
    vector<int> v4{1, -2147483648, -2147483648};

    assert(s.intersect(v1, v2) == (vector<int>{2, 2}));

    auto v5 = s.intersect(v3, v4);
    unordered_set<int> s1(v5.begin(), v5.end());
    unordered_set<int> s2{1, -2147483648};

    assert(s1 == s2);
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
