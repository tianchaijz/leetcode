#include "common.h"

class Solution {
  public:
    int reverse(int x) {
        long y = 0;
        for (; x; x /= 10) {
            y = y * 10 + x % 10;
        }

        return (y < INT_MIN || y > INT_MAX) ? 0 : y;
    }
};

TEST_CASE {
    Solution s;

    assert(s.reverse(321) == 123);
    assert(s.reverse(-321) == -123);
    assert(s.reverse(1534236469) == 0);
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    printf("sizeof(int): %lu, sizeof(long): %lu\n", sizeof(int), sizeof(long));

    return 0;
}
