#include "common.h"

class Solution {
  public:
    int findNthDigit(int n) {
        int digits = 1, base = 1;
        for (--n; n >= 9L * base * digits; ++digits, base *= 10) {
            n -= 9L * base * digits;
        }

        // find the nth number
        string s = to_string(base + n / digits);

        return s[n % digits] - '0';
    }
};

TEST_CASE {
    Solution s;
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    Solution s;
    std::cout << s.findNthDigit(3) << std::endl;
    std::cout << s.findNthDigit(11) << std::endl;
    std::cout << s.findNthDigit(1000000000) << std::endl;

    return 0;
}
