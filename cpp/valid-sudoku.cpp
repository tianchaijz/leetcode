#include "common.h"

class Solution {
  public:
    bool isValidSudoku(vector<vector<char>> &board) {
        int r[9][9] = {}, c[9][9] = {}, b[9][9] = {};
        for (int i = 0; i < 9; ++i) {
            for (int j = 0; j < 9; ++j) {
                if (board[i][j] != '.') {
                    int n = board[i][j] - '1';
                    if (r[i][n]++ || c[j][n]++ || b[i / 3 * 3 + j / 3][n]++)
                        return false;
                }
            }
        }
        return true;
    }
};

TEST_CASE {
    Solution s;
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
