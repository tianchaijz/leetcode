#include "common.h"

class Solution {
  public:
    int mySqrt(int x) {
        long r = x;
        while (r * r > x)
            r = (r + x / r) / 2;
        return r;
    }
};

TEST_CASE {
    Solution s;

    assert(s.mySqrt(1) == 1);
    assert(s.mySqrt(4) == 2);
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
