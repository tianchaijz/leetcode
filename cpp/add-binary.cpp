#include "common.h"

class Solution {
  public:
    string addBinary(string a, string b) {
        string s;
        int carry = 0, i = a.size() - 1, j = b.size() - 1;
        while (i >= 0 || j >= 0 || carry) {
            carry += i >= 0 ? a[i--] - '0' : 0;
            carry += j >= 0 ? b[j--] - '0' : 0;
            s = static_cast<char>(carry % 2 + '0') + s;
            carry /= 2;
        }

        return s;
    }
};

TEST_CASE {
    Solution s;

    assert(s.addBinary("0", "0") == "0");
    assert(s.addBinary("0", "1") == "1");
    assert(s.addBinary("10", "1") == "11");
    assert(s.addBinary("11", "1") == "100");
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
