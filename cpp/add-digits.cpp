#include "common.h"

class Solution {
  public:
    int addDigits(int num) {
        return 1 + (num - 1) % 9;
    }
};

TEST_CASE {
    Solution s;

    assert(s.addDigits(38) == 2);
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
