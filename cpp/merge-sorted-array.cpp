#include "common.h"

class Solution {
  public:
    void merge(vector<int> &nums1, int m, vector<int> &nums2, int n) {
        for (int i = m - 1, j = n - 1, k = m + n - 1; j >= 0; --k) {
            nums1[k] = i >= 0 && nums1[i] > nums2[j] ? nums1[i--] : nums2[j--];
        }
    }
};

class Solution1 {
  public:
    void merge(vector<int> &A, int m, vector<int> &B, int n) {
        int k = m + n - 1;
        while (n) {
            // http://en.cppreference.com/w/cpp/language/eval_order
            A[k--] = m && A[m - 1] > B[n - 1] ? A[--m] : B[--n];
        }
    }
};

TEST_CASE {
    Solution s;

    vector<int> v1{1, 2, 3, 5};
    vector<int> v2{1, 1, 4};

    v1.resize(v1.size() + v2.size());
    s.merge(v1, 4, v2, 3);
    assert(v1 == (vector<int>{1, 1, 1, 2, 3, 4, 5}));
}

int main(int argc, char *argv[]) {
    TEST(Solution);
    TEST(Solution1);

    return 0;
}
