#define TEST_CALLS 1
#include "common.h"

class Solution {
  public:
    vector<int> getRow(int rowIndex) {
        vector<int> base(1, 1);
        vector<int> row;
        for (int i = 0; i < rowIndex + 1; ++i) {
            row.resize(i + 1);
            row[0] = row[i] = 1;
            for (int j = 1; j < i; ++j) {
                row[j] = base[j] + base[j - 1];
            }
            base = row;
        }

        return row;
    }
};

class Solution1 {
  public:
    vector<int> getRow(int rowIndex) {
        vector<int> row(rowIndex + 1, 1);
        for (int i = 0; i < rowIndex; ++i) {
            for (int j = i; j > 0; --j) {
                row[j] += row[j - 1];
            }
        }

        return row;
    }
};

TEST_CASE {
    Solution s;

    std::cout << s.getRow(0) << std::endl;
    std::cout << s.getRow(1) << std::endl;
    std::cout << s.getRow(2) << std::endl;
    std::cout << s.getRow(3) << std::endl;
}

int main(int argc, char *argv[]) {
    TEST(Solution);
    TEST(Solution1);

    return 0;
}
