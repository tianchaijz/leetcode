#include "common.h"

class Solution {
  public:
    int numberOfArithmeticSlices(vector<int> &A) {
        if (A.size() < 3)
            return 0;
        vector<int> dp(A.size(), 0);
        int n = 0;
        for (size_t i = 2; i < A.size(); ++i) {
            if (A[i] - A[i - 1] == A[i - 1] - A[i - 2])
                dp[i] = dp[i - 1] + 1;
            n += dp[i];
        }

        return n;
    }
};

class Solution1 {
  public:
    int numberOfArithmeticSlices(vector<int> &A) {
        int n = 0, count = 0;
        for (size_t i = 2; i < A.size(); ++i) {
            if (A[i] - A[i - 1] == A[i - 1] - A[i - 2]) {
                count += 1;
                n += count;
            } else {
                count = 0;
            }
        }

        return n;
    }
};

TEST_CASE {
    Solution s;
    vector<int> v1{1, 2, 3, 4};
    vector<int> v2{1, 3, 5, 7, 9};

    assert(s.numberOfArithmeticSlices(v1) == 3);
    assert(s.numberOfArithmeticSlices(v2) == 6);
}

int main(int argc, char *argv[]) {
    TEST(Solution);
    TEST(Solution1);

    return 0;
}
