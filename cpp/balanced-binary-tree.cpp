#include "common.h"

class Solution {
  public:
    bool isBalanced(TreeNode *root) {
        return balancedHeight(root) >= 0;
    }

  private:
    int balancedHeight(TreeNode *root) {
        if (root == nullptr)
            return 0;

        int left = balancedHeight(root->left);
        int right = balancedHeight(root->right);

        if (left < 0 || right < 0 || abs(left - right) > 1)
            return -1;

        return max(left, right) + 1;
    }
};

TEST_CASE {
    Solution s;
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
