#include "common.h"

class Solution {
  public:
    int firstUniqChar(string s) {
        int c[256] = {0};
        for (unsigned char i : s)
            ++c[i];
        for (int i = 0; i < s.size(); ++i) {
            if (c[s[i] - '\0'] == 1)
                return i;
        }
        return -1;
    }
};

TEST_CASE {
    Solution s;

    assert(s.firstUniqChar("leetcode") == 0);
    assert(s.firstUniqChar("loveleetcode") == 2);
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
