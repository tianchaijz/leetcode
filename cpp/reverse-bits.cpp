#include "common.h"

class Solution {
  public:
    uint32_t reverseBits(uint32_t n) {
        for (size_t i = 0, j = sizeof(uint32_t) * 8 - 1; i < j; ++i, --j) {
            uint32_t x = (n >> i) & 1, y = (n >> j) & 1;
            n = (n & ~(1 << i)) | (y << i);
            n = (n & ~(1 << j)) | (x << j);
        }

        return n;
    }
};

class Solution1 {
  public:
    uint32_t reverseBits(uint32_t n) {
        uint32_t m = 0;
        for (size_t i = 0; i < 32; ++i, n >>= 1) {
            m <<= 1;
            m |= n & 1;
        }

        return m;
    }
};

TEST_CASE {
    Solution s;

    assert(s.reverseBits(1) == 1 << 31);
    assert(s.reverseBits(0xFFFFFFFF) == 0xFFFFFFFF);
}

int main(int argc, char *argv[]) {
    TEST(Solution);
    TEST(Solution1);

    return 0;
}
