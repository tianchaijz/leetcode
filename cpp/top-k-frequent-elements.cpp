#include "common.h"

// https://discuss.leetcode.com/topic/44226/c-o-n-log-n-k-unordered_map-and-priority_queue-maxheap-solution
class Solution {
  public:
    vector<int> topKFrequent(vector<int> &nums, int k) {
        unordered_map<int, int> f;
        vector<int> r;
        priority_queue<pair<int, int>> pq;

        for (auto &n : nums)
            ++f[n];
        for (auto &x : f) {
            pq.push({x.second, x.first});
            if (static_cast<int>(pq.size()) > static_cast<int>(f.size()) - k) {
                r.push_back(pq.top().second);
                pq.pop();
            }
        }

        sort(r.begin(), r.end());

        return r;
    }
};

class Solution1 {
  public:
    vector<int> topKFrequent(vector<int> &nums, int k) {
        int mf = 0;
        unordered_map<int, int> f;
        vector<vector<int>> c;
        vector<int> r;

        for (auto &n : nums)
            mf = max(mf, ++f[n]);
        c.resize(mf + 1);
        for (auto &x : f)
            c[x.second].push_back(x.first);
        for (size_t i = c.size() - 1; i > 0 && r.size() < k; --i) {
            if (c[i].empty())
                continue;
            for (auto &n : c[i])
                r.push_back(n);
        }

        sort(r.begin(), r.end());

        return r;
    }
};

TEST_CASE {
    Solution s;

    vector<int> v1{1, 1, 1, 2, 2, 3}, v2{-1, -1}, v3{1, 2};
    assert(s.topKFrequent(v1, 2) == (vector<int>{1, 2}));
    assert(s.topKFrequent(v2, 1) == (vector<int>{-1}));
    assert(s.topKFrequent(v3, 2) == (vector<int>{1, 2}));
}

int main(int argc, char *argv[]) {
    TEST(Solution);
    TEST(Solution1);

    return 0;
}
