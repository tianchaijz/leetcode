#include "common.h"

class Solution {
  public:
    ListNode *deleteDuplicates(ListNode *head) {
        ListNode *h = head;
        for (; head; head = head->next) {
            ListNode *next = head->next;
            while (next && head->val == next->val) {
                head->next = next->next;
                delete next;
                next = head->next;
            }
        }

        return h;
    }
};

TEST_CASE {
    Solution s;
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
