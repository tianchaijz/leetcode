#include "common.h"

class Solution {
  public:
    vector<string> readBinaryWatch(int num) {
        if (num > 10)
            num = 10;

        vector<bool> clock(10, 0);
        for (int i = 0; i < num; ++i)
            clock[i] = 1;

        sort(clock.begin(), clock.end());

        vector<string> r;
        do {
            int h = 0, m = 0;

            for (int i = 0, j = 3; i < 4; ++i) {
                if (clock[i])
                    h |= clock[i] << j;
                --j;
            }

            for (int i = 4, j = 5; i < 10; ++i) {
                if (clock[i])
                    m |= clock[i] << j;
                --j;
            }

            if (h > 11 || m > 59)
                continue;

            char buf[6];
            sprintf(buf, "%d:%02d", h, m);
            r.emplace_back(buf);
        } while (std::next_permutation(clock.begin(), clock.end()));

        return r;
    }
};

class Solution1 {
  public:
    vector<string> readBinaryWatch(int num) {
        vector<string> r;
        dfs(r, num, 0, 0, 0);
        return r;
    }

  private:
    void dfs(vector<string> &r, int n, int h, int m, int pos) {
        if (h > 11 || m > 59)
            return;
        if (n == 0) {
            r.emplace_back(to_string(h) + (m < 10 ? ":0" : ":") + to_string(m));
            return;
        }

        for (int p = pos; p < 10; ++p) {
            if (p < 6)
                dfs(r, n - 1, h, m | (1 << p), p + 1);
            else
                dfs(r, n - 1, h | (1 << (p - 6)), m, p + 1);
        }
    }
};

class Answer {
  public:
    vector<string> readBinaryWatch(int num) {
        vector<string> r;
        for (int h = 0; h < 12; h++)
            for (int m = 0; m < 60; m++)
                if (bitset<10>(h << 6 | m).count() == num)
                    r.emplace_back(to_string(h) + (m < 10 ? ":0" : ":") + to_string(m));
        return r;
    }
};

TEST_CASE {
    Answer a;
    Solution s;

    for (int i = 1; i < 11; ++i) {
        vector<string> v1 = a.readBinaryWatch(i);
        vector<string> v2 = s.readBinaryWatch(i);

        unordered_set<string> s1(v1.begin(), v1.end());
        unordered_set<string> s2(v2.begin(), v2.end());

        assert(v1 == v2 || s1 == s2);
    }
}

int main(int argc, char *argv[]) {
    TEST(Solution);
    TEST(Solution1);

    return 0;
}
