#include "common.h"

class Solution {
  public:
    int numberOfBoomerangs(vector<pair<int, int>> &points) {
        int r = 0;
        for (auto &p : points) {
            unordered_map<double, int> hash(points.size());
            for (auto &q : points)
                r += 2 * hash[hypot(p.first - q.first, p.second - q.second)]++;
        }

        return r;
    }
};

class Solution1 {
  public:
    int numberOfBoomerangs(vector<pair<int, int>> &points) {
        int r = 0;
        for (auto &p : points) {
            vector<int> d;
            for (auto &q : points) {
                if (p != q)
                    d.emplace_back(dis(p, q));
            }

            sort(d.begin(), d.end());

            for (int i = 0, j = 0; j < d.size(); j = i) {
                for (; i < d.size() && d[j] == d[i]; ++i)
                    ;
                r += (i - j) * (i - j - 1);
            }
        }

        return r;
    }

  private:
    int dis(pair<int, int> &x, pair<int, int> &y) {
        int i = x.first - y.first, j = x.second - y.second;
        return i * i + j * j;
    }
};

TEST_CASE {
    Solution s;
    vector<pair<int, int>> p1 = {{0, 0}, {1, 0}, {2, 0}};
    vector<pair<int, int>> p2 = {{0, 0}, {1, 0}, {-1, 0}, {0, 1}, {0, -1}};

    assert(s.numberOfBoomerangs(p1) == 2);
    assert(s.numberOfBoomerangs(p2) == 20);
}

int main(int argc, char *argv[]) {
    TEST(Solution);
    TEST(Solution1);

    return 0;
}
