#include "common.h"

class Solution {
  public:
    int majorityElement(vector<int> &a) {
        int major = a[0], count = 0;
        for (auto n : a) {
            if (!count) {
                ++count;
                major = n;
            } else if (major == n)
                ++count;
            else
                --count;
        }

        return major;
    }
};

class Solution1 {
  public:
    int majorityElement(vector<int> &nums) {
        unordered_map<int, int> m;
        int size = nums.size() / 2;
        for (int n : nums) {
            ++m[n];
            if (m[n] > size)
                return n;
        }
        return 0;
    }
};

TEST_CASE {
    Solution s;

    vector<int> v1{1, 2, 2, 2};
    vector<int> v2{1, 1, 2, 2, 2};

    assert(s.majorityElement(v1) == 2);
    assert(s.majorityElement(v2) == 2);
}

int main(int argc, char *argv[]) {
    TEST(Solution);
    TEST(Solution1);

    return 0;
}
