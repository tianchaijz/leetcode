#include "common.h"

class Solution {
  public:
    vector<int> intersection(vector<int> &a, vector<int> &b) {
        sort(a.begin(), a.end());
        sort(b.begin(), b.end());

        vector<int> r;
        auto i = a.begin(), j = b.begin();
        while (i != a.end() && j != b.end()) {
            if (*i < *j)
                ++i;
            else if (*i > *j)
                ++j;
            else {
                r.push_back(*i);
                while (++i != a.end() && *i == r.back())
                    ;
                while (++j != b.end() && *j == r.back())
                    ;
            }
        }

        return r;
    }
};

class Solution1 {
  public:
    vector<int> intersection(vector<int> &a, vector<int> &b) {
        unordered_set<int> s(a.begin(), a.end());
        vector<int> r;
        for (int n : b) {
            if (s.count(n)) {
                r.push_back(n);
                s.erase(n);
            }
        }

        return r;
    }
};

TEST_CASE {
    Solution s;

    vector<int> v1{1, 2, 2, 1};
    vector<int> v2{2, 2};

    assert(s.intersection(v1, v2) == vector<int>{2});
}

int main(int argc, char *argv[]) {
    TEST(Solution);
    TEST(Solution1);

    return 0;
}
