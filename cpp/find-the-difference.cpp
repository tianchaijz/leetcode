#include "common.h"

class Solution {
  public:
    char findTheDifference(string s, string t) {
        sort(s.begin(), s.end());
        sort(t.begin(), t.end());

        int lo = 0, hi = t.size() - 1;
        while (lo < hi) {
            int span = (hi - lo + 1) / 2, mid = lo + span - 1;
            if (s.substr(lo, span) == t.substr(lo, span))
                lo = mid + 1;
            else
                hi = mid - 1;
        }

        return t[hi];
    }
};

class Solution1 {
  public:
    char findTheDifference(string s, string t) {
        long x = 0;
        for (char i : t)
            x ^= i;
        for (char i : s)
            x ^= i;
        return x;
    }
};

class Solution2 {
  public:
    char findTheDifference(string s, string t) {
        char diff = 0;
        int count[26] = {0};
        for (char c : s)
            count[c - 'a']++;
        for (char c : t)
            if (--count[c - 'a'] < 0)
                return c;
        return diff;
    }
};

TEST_CASE {
    Solution s;

    assert(s.findTheDifference("ae", "aea") == 'a');
    assert(s.findTheDifference("abcd", "abcde") == 'e');
    assert(s.findTheDifference("abcd", "eabcd") == 'e');
    assert(s.findTheDifference("abcd", "aebcd") == 'e');
    assert(s.findTheDifference("abcd", "abced") == 'e');
}

int main(int argc, char *argv[]) {
    TEST(Solution);
    TEST(Solution1);
    TEST(Solution2);

    return 0;
}
