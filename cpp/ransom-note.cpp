#include "common.h"

class Solution {
  public:
    bool canConstruct(string ransomNote, string magazine) {
        int c[256] = {0};
        for (unsigned char i : magazine) {
            c[i]++;
        }
        for (unsigned char i : ransomNote) {
            c[i]--;
        }
        for (int i : c)
            if (i < 0)
                return false;

        return true;
    }
};

TEST_CASE {
    Solution s;

    assert(s.canConstruct("a", "b") == false);
    assert(s.canConstruct("aa", "ab") == false);
    assert(s.canConstruct("aa", "aab") == true);
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
