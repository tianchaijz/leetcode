#!/bin/sh

commit=$1

git status
if [ -n "$commit" ]; then
    git add . && git commit
fi
