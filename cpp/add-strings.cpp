#include "common.h"

class Solution {
  public:
    string addStrings(string a, string b) {
        string sum = "";
        int c = 0;
        for (int i = a.size(), j = b.size(); i > 0 || j > 0;) {
            if (i)
                c += a[--i] - '0';
            if (j)
                c += b[--j] - '0';

            sum += '0' + c % 10;
            c /= 10;
        }

        if (c)
            sum += '1';

        reverse(sum.begin(), sum.end());

        return sum;
    }
};

class Solution1 {
  public:
    string addStrings(string a, string b) {
        string sum;
        int m = a.size(), n = b.size(), c = 0;
        for (int i = 0; i < max(m, n); ++i) {
            if (i < m)
                c += a[m - 1 - i] - '0';
            if (i < n)
                c += b[n - 1 - i] - '0';

            sum += '0' + c % 10;
            c /= 10;
        }

        if (c)
            sum += '1';

        reverse(sum.begin(), sum.end());

        return sum;
    }
};

TEST_CASE {
    Solution s;

    string s1 = {'1', '2'};
    string s2 = {'1'};
    string s3 = {'0'};
    string s4 = {'9'};

    assert(s.addStrings(s1, s2) == "13");
    assert(s.addStrings(s2, s3) == "1");
    assert(s.addStrings(s3, s3) == "0");
    assert(s.addStrings(s2, s4) == "10");
}

int main(int argc, char *argv[]) {
    TEST(Solution);
    TEST(Solution1);

    return 0;
}
