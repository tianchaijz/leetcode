#define TEST_CALLS 1
#include "common.h"

class Solution {
  public:
    string countAndSay(int n) {
        if (n == 0)
            return "";

        string s = "1";
        while (--n) {
            string ss;
            for (size_t i = 0; i < s.size(); ++i) {
                int count = 1;
                for (; i + 1 < s.size() && s[i] == s[i + 1]; ++i, ++count)
                    ;
                ss += to_string(count) + s[i];
            }

            s = ss;
        }

        return s;
    }
};

TEST_CASE {
    Solution s;

    std::cout << s.countAndSay(1) << std::endl;
    std::cout << s.countAndSay(2) << std::endl;
    std::cout << s.countAndSay(3) << std::endl;
    std::cout << s.countAndSay(4) << std::endl;
    std::cout << s.countAndSay(5) << std::endl;
    std::cout << s.countAndSay(6) << std::endl;
    std::cout << s.countAndSay(7) << std::endl;
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
