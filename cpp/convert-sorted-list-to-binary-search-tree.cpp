#include "common.h"

class Solution {
  public:
    TreeNode *sortedListToBST(ListNode *head) {
        std::vector<ListNode *> v;
        while (head) {
            v.push_back(head);
            head = head->next;
        }

        return sortedArrayToBST(v, 0, v.size() - 1);
    }

  private:
    TreeNode *sortedArrayToBST(std::vector<ListNode *> &v, int lo, int hi) {
        if (lo > hi)
            return nullptr;

        int mid = lo + (hi - lo) / 2;
        auto root = new TreeNode(v[mid]->val);
        root->left = sortedArrayToBST(v, lo, mid - 1);
        root->right = sortedArrayToBST(v, mid + 1, hi);

        return root;
    }
};

class Solution1 {
  public:
    TreeNode *sortedListToBST(ListNode *head) {
        head_ = head;
        int len = 0;
        while (head) {
            ++len;
            head = head->next;
        }

        return aux(len);
    }

  private:
    ListNode *head_;
    TreeNode *aux(int n) {
        if (n == 0)
            return nullptr;
        auto root = new TreeNode(0);
        root->left = aux(n / 2);
        root->val = head_->val;
        head_ = head_->next;
        root->right = aux(n - n / 2 - 1);

        return root;
    }
};

TEST_CASE {
    Solution s;
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
