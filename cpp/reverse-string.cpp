#include "common.h"

class Solution {
  public:
    string reverseString(string s) {
        int size = s.size();
        for (int i = 0; i < size / 2; ++i) {
            swap(s[i], s[size - 1 - i]);
        }

        return s;
    }
};

class Solution1 {
  public:
    string reverseString(string s) {
        int i = 0, j = s.size() - 1;
        while (i < j) {
            swap(s[i++], s[j--]);
        }

        return s;
    }
};

TEST_CASE {
    Solution s;

    assert(s.reverseString("") == "");
    assert(s.reverseString("a") == "a");
    assert(s.reverseString("ab") == "ba");
    assert(s.reverseString("abc") == "cba");
}

int main(int argc, char *argv[]) {
    TEST(Solution);
    TEST(Solution1);

    return 0;
}
