#include "common.h"

class Solution {
  public:
    int removeDuplicates(vector<int> &nums) {
        int size = nums.size();
        if (size == 0)
            return 0;
        int n = nums[0], i = 1;
        for (int j = 1; j < size; ++i) {
            for (; j < size && nums[j] == n; ++j)
                ;
            if (j >= size)
                break;
            n = nums[i] = nums[j];
        }

        return i;
    }
};

class Solution1 {
  public:
    int removeDuplicates(vector<int> &nums) {
        int size = nums.size();
        if (size == 0)
            return 0;
        int i = 1;
        for (int j = 1; j < size; ++j) {
            if (nums[j] != nums[j - 1])
                nums[i++] = nums[j];
        }

        return i;
    }
};

class Solution2 {
  public:
    int removeDuplicates(vector<int> &nums) {
        int i = 0;
        for (auto n : nums) {
            if (!i || nums[i - 1] != n)
                nums[i++] = n;
        }

        return i;
    }
};

TEST_CASE {
    Solution s;
    vector<int> v1{1, 1, 2};
    vector<int> v2{1, 1, 1};
    vector<int> v3{1, 2, 3};

    assert(s.removeDuplicates(v1) == 2);
    assert(s.removeDuplicates(v2) == 1);
    assert(s.removeDuplicates(v3) == 3);
}

int main(int argc, char *argv[]) {
    TEST(Solution);
    TEST(Solution1);
    TEST(Solution2);

    return 0;
}
