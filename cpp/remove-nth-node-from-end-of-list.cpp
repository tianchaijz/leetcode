#define TEST_CALLS 1
#include "common.h"

class Solution {
  public:
    ListNode *removeNthFromEnd(ListNode *head, int n) {
        ListNode *h = head;
        queue<ListNode *> q;
        while (h) {
            q.push(h);
            if (static_cast<int>(q.size()) > n + 1)
                q.pop();
            h = h->next;
        }

        // remove head
        if (static_cast<int>(q.size()) == n) {
            return q.front()->next;
        } else if (static_cast<int>(q.size()) == n + 1) {
            ListNode *l = q.front();
            q.pop();
            l->next = q.front()->next;
        }

        return head;
    }
};

class Solution1 {
  public:
    ListNode *removeNthFromEnd(ListNode *head, int n) {
        ListNode **curr = &head, *h = head;
        while (n--) {
            h = h->next;
        }
        while (h) {
            h = h->next;
            curr = &(*curr)->next;
        }

        h = (*curr)->next;
        // delete *curr;
        *curr = h;

        return head;
    }
};

TEST_CASE {
    Solution s;

    ListNode a(1);
    ListNode b(2, &a);
    ListNode c(3);

    printList(s.removeNthFromEnd(&b, 1));
    printList(s.removeNthFromEnd(&c, 1));
}

int main(int argc, char *argv[]) {
    TEST(Solution);
    TEST(Solution1);

    return 0;
}
