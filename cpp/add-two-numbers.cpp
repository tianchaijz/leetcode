#include "common.h"

class Solution {
  public:
    ListNode *addTwoNumbers(ListNode *l1, ListNode *l2) {
        return helper(l1, l2, 0);
    }

  private:
    ListNode *helper(ListNode *l1, ListNode *l2, int n) {
        ListNode *l = NULL;

        if (l1) {
            n += l1->val;
            l1 = l1->next;
        }

        if (l2) {
            n += l2->val;
            l2 = l2->next;
        }

        l = new ListNode(n % 10);
        n /= 10;

        if (l1 || l2 || n) {
            l->next = helper(l1, l2, n);
        }

        return l;
    }
};

class Solution1 {
  public:
    ListNode *addTwoNumbers(ListNode *l1, ListNode *l2) {
        ListNode *head = NULL, *p = NULL;
        int n = 0;

        while (l1 || l2) {
            if (l1) {
                n += l1->val;
                l1 = l1->next;
            }

            if (l2) {
                n += l2->val;
                l2 = l2->next;
            }

            ListNode *l = new ListNode(n % 10);
            n /= 10;

            if (p) {
                p->next = l;
                p = l;
            } else {
                p = head = l;
            }
        }

        if (n)
            p->next = new ListNode(n);

        return head;
    }
};

ListNode *numberToListNode(int num, ListNode *l) {
    ListNode *ln = new ListNode(num % 10);
    ln->next = l;

    num /= 10;
    if (num) {
        return numberToListNode(num, ln);
    }

    return ln;
}

void printListNode(ListNode *l) {
    while (l) {
        printf("%d ", l->val);
        l = l->next;
    }

    printf("\n");
}

int listNodeToNumber(ListNode *l) {
    int num = 0, base = 1;
    while (l) {
        num = l->val * base + num;
        base *= 10;
        l = l->next;
    }

    return num;
}

TEST_CASE {
    Solution s;

    ListNode *l1 = numberToListNode(243, NULL);
    ListNode *l2 = numberToListNode(564, NULL);

    ListNode *l3 = numberToListNode(5, NULL);
    ListNode *l4 = numberToListNode(5, NULL);

    ListNode *l5 = numberToListNode(0, NULL);
    ListNode *l6 = numberToListNode(0, NULL);

    assert(listNodeToNumber(s.addTwoNumbers(l1, l2)) == 807);
    assert(listNodeToNumber(s.addTwoNumbers(l3, l4)) == 10);
    assert(listNodeToNumber(s.addTwoNumbers(l5, l6)) == 0);
}

int main(int argc, char *argv[]) {
    TEST(Solution);
    TEST(Solution1);

    return 0;
}
