#include "common.h"

class Solution {
  public:
    string getHint(string secret, string guess) {
        if (secret.size() != guess.size() || secret.empty())
            return "";

        int c[10] = {};
        for (auto &a : secret)
            ++c[a - '0'];

        size_t bulls = 0, cows = 0;
        for (size_t i = 0; i < secret.size(); ++i) {
            if (secret[i] == guess[i]) {
                ++bulls;
                if (--c[secret[i] - '0'] < 0)
                    --cows;
            } else if (c[guess[i] - '0'] > 0) {
                --c[guess[i] - '0'];
                ++cows;
            }
        }

        return to_string(bulls) + "A" + to_string(cows) + "B";
    }
};

class Solution1 {
  public:
    string getHint(string secret, string guess) {
        int c[10] = {}, bulls = 0, cows = 0;
        for (size_t i = 0; i < secret.size(); ++i) {
            if (secret[i] == guess[i])
                ++bulls;
            else {
                if (c[secret[i] - '0']++ < 0)
                    ++cows;
                if (c[guess[i] - '0']-- > 0)
                    ++cows;
            }
        }

        return to_string(bulls) + "A" + to_string(cows) + "B";
    }
};

TEST_CASE {
    Solution s;

    assert(s.getHint("1807", "7810") == "1A3B");
    assert(s.getHint("1123", "0111") == "1A1B");
    assert(s.getHint("1122", "1222") == "3A0B");
}

int main(int argc, char *argv[]) {
    TEST(Solution);
    TEST(Solution1);

    std::srand(std::time(0));

    Solution s;
    Solution1 s1;
    for (int i = 0; i < 1000000; ++i) {
        size_t size = std::rand() % 7 + 4;

        string secret(size, '0');
        string guess(size, '0');

        std::generate(secret.begin(), secret.end(), [] { return '0' + std::rand() % 10; });
        std::generate(guess.begin(), guess.end(), [] { return '0' + std::rand() % 10; });

        assert(s.getHint(secret, guess) == s1.getHint(secret, guess));
    }

    return 0;
}
