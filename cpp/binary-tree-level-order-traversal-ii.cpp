#define TEST_CALLS 1
#include "common.h"

class Solution {
  public:
    vector<vector<int>> levelOrderBottom(TreeNode *root) {
        vector<vector<int>> r;
        bfs(root, r);
        reverse(r.begin(), r.end());
        return r;
    }

  private:
    void bfs(TreeNode *root, vector<vector<int>> &r) {
        if (root == NULL)
            return;
        queue<TreeNode *> q;
        q.push(root);
        while (!q.empty()) {
            vector<int> v;
            for (int i = 0, j = q.size(); i < j; ++i) {
                TreeNode *node = q.front();
                q.pop();
                v.push_back(node->val);
                if (node->left)
                    q.push(node->left);
                if (node->right)
                    q.push(node->right);
            }
            r.push_back(v);
        }
    }
};

class Solution1 {
  public:
    vector<vector<int>> levelOrderBottom(TreeNode *root) {
        vector<vector<int>> r;
        dfs(root, 0, r);
        reverse(r.begin(), r.end());
        return r;
    }

  private:
    void dfs(TreeNode *root, int height, vector<vector<int>> &r) {
        if (root == NULL)
            return;
        while (r.size() <= height)
            r.emplace_back(vector<int>());
        r[height].push_back(root->val);
        dfs(root->left, height + 1, r);
        dfs(root->right, height + 1, r);
    }
};

// DFS + Stack
class Solution2 {
  public:
    vector<vector<int>> levelOrderBottom(TreeNode *root) {
        vector<vector<int>> r;
        dfs(root, 0, r);
        reverse(r.begin(), r.end());
        return r;
    }

  private:
    void dfs(TreeNode *root, int height, vector<vector<int>> &r) {
        if (root == NULL)
            return;
        stack<pair<TreeNode *, int>> s;
        s.push(make_pair(root, 0));
        while (!s.empty()) {
            auto p = s.top();
            s.pop();
            if (r.size() <= p.second)
                r.emplace_back();
            r[p.second].push_back(p.first->val);
            if (p.first->right)
                s.push(make_pair(p.first->right, p.second + 1));
            if (p.first->left)
                s.push(make_pair(p.first->left, p.second + 1));
        }
    }
};

TEST_CASE {
    Solution s;

    TreeNode a{0};
    TreeNode b{1};
    TreeNode c{3};
    TreeNode d{4};
    TreeNode e{5, &a, &b};
    TreeNode f{6, &c, &d};
    TreeNode g{7, &e, &f};

    auto r = s.levelOrderBottom(&g);
    for (auto v : r) {
        std::cout << r << std::endl;
    }
}

int main(int argc, char *argv[]) {
    TEST(Solution);
    TEST(Solution1);
    TEST(Solution2);

    return 0;
}
