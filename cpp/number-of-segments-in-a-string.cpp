#include "common.h"

class Solution {
  public:
    int countSegments(string s) {
        int c = 0, i = 0;
        while (i < s.size()) {
            if (s[i] != ' ') {
                ++c, ++i;
                for (; i < s.size() && s[i] != ' '; ++i)
                    ;
            }

            for (; i < s.size() && s[i] == ' '; ++i)
                ;
        }

        return c;
    }
};

class Solution1 {
  public:
    int countSegments(string s) {
        int c = s.size() && s.back() != ' ';
        for (size_t i = 1; i < s.size(); ++i) {
            if (s[i - 1] != ' ' && s[i] == ' ')
                ++c;
        }

        return c;
    }
};

TEST_CASE {
    Solution s;

    assert(s.countSegments("1") == 1);
    assert(s.countSegments("1 ") == 1);
    assert(s.countSegments("1 2") == 2);
    assert(s.countSegments("1 2 ") == 2);
    assert(s.countSegments(" 1 2 ") == 2);
}

int main(int argc, char *argv[]) {
    TEST(Solution);
    TEST(Solution1);

    return 0;
}
