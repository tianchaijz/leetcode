#include "common.h"

class Solution {
  public:
    string reverseVowels(string s) {
        unordered_map<char, bool> m;
        for (char c : "aeiouAEIOU")
            m[c] = true;

        for (int i = 0, j = s.size() - 1; i < j;) {
            if (m.find(s[i]) == m.end())
                ++i;
            else if (m.find(s[j]) == m.end())
                --j;
            else
                swap(s[i++], s[j--]);
        }

        return s;
    }
};

class Solution1 {
  public:
    string reverseVowels(string s) {
        int i = 0, j = s.size() - 1;
        while (i < j) {
            i = s.find_first_of("aeiouAEIOU", i);
            j = s.find_last_of("aeiouAEIOU", j);
            if (i < j)
                swap(s[i++], s[j--]);
        }

        return s;
    }
};

class Solution2 {
  public:
    string reverseVowels(string s) {
        int i = 0, j = s.size() - 1;
        string vowels = "aeiouAEIOU";
        while (i < j) {
            while (vowels.find(s[i]) == string::npos && i < j)
                ++i;
            while (vowels.find(s[j]) == string::npos && i < j)
                --j;
            if (i < j)
                swap(s[i++], s[j--]);
        }

        return s;
    }
};

TEST_CASE {
    Solution s;

    assert(s.reverseVowels("aA") == "Aa");
    assert(s.reverseVowels("hello") == "holle");
    assert(s.reverseVowels("leetcode") == "leotcede");
}

int main(int argc, char *argv[]) {
    TEST(Solution);
    TEST(Solution1);
    TEST(Solution2);

    return 0;
}
