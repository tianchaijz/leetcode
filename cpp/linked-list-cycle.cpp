#include "common.h"

class Solution {
  public:
    bool hasCycle(ListNode *head) {
        ListNode *slow = head, *fast = head;
        while (fast && fast->next) {
            slow = slow->next;
            fast = fast->next->next;
            if (slow == fast)
                return true;
        }

        return false;
    }
};

class Solution1 {
  public:
    bool hasCycle(ListNode *head) {
        unordered_set<ListNode *> s;
        while (head && !s.count(head)) {
            s.insert(head);
            head = head->next;
        }

        return head;
    }
};

TEST_CASE {
    Solution s;
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
