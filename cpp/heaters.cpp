#include "common.h"

class Solution {
  public:
    int findRadius(vector<int> &houses, vector<int> &heaters) {
        sort(houses.begin(), houses.end());
        sort(heaters.begin(), heaters.end());

        int r = 0;
        auto it = heaters.begin();
        for (auto h : houses) {
            // lower_bound
            while (it != heaters.end() && *it < h)
                ++it;
            int t = it != heaters.end() ? *it - h : INT_MAX;
            if (it != heaters.begin())
                t = min(t, h - it[-1]);
            r = max(r, t);
        }

        return r;
    }
};

TEST_CASE {
    Solution s;

    vector<int> houses{1, 2, 3, 4}, heaters{1, 4};
    vector<int> houses1{1, 2, 3}, heaters1{2};
    vector<int> houses2{1, 5}, heaters2{2};
    vector<int> houses3{1, 5}, heaters3{10};
    vector<int> houses4{1}, heaters4{1, 2, 3, 4};
    vector<int> houses5{1, 2, 3}, heaters5{1, 2, 3};

    assert(s.findRadius(houses, heaters) == 1);
    assert(s.findRadius(houses1, heaters1) == 1);
    assert(s.findRadius(houses2, heaters2) == 3);
    assert(s.findRadius(houses3, heaters3) == 9);
    assert(s.findRadius(houses4, heaters4) == 0);
    assert(s.findRadius(houses5, heaters5) == 0);
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
