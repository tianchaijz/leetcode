#include "common.h"

class Solution {
  public:
    vector<int> findDuplicates(vector<int> &nums) {
        vector<int> r;
        for (size_t i = 0; i < nums.size(); ++i) {
            size_t idx = abs(nums[i]) - 1;
            if (nums[idx] < 0)
                r.push_back(idx + 1);
            nums[idx] = -nums[idx];
        }

        return r;
    }
};

class Solution1 {
  public:
    vector<int> findDuplicates(vector<int> &nums) {
        vector<int> r;
        for (size_t i = 0; i < nums.size();)
            if (nums[i] != nums[nums[i] - 1])
                swap(nums[i], nums[nums[i] - 1]);
            else
                ++i;
        for (size_t i = 0; i < nums.size(); ++i)
            if (nums[i] != static_cast<int>(i) + 1)
                r.push_back(nums[i]);

        sort(r.begin(), r.end());

        return r;
    }
};

TEST_CASE {
    Solution s;

    vector<int> v{4, 3, 2, 7, 8, 2, 3, 1};
    assert(s.findDuplicates(v) == (vector<int>{2, 3}));
}

int main(int argc, char *argv[]) {
    TEST(Solution);
    TEST(Solution1);

    return 0;
}
