#include "common.h"

class Solution {
  public:
    vector<string> fizzBuzz(int n) {
        vector<string> r(n);
        for (int i = 1; i <= n; ++i) {
            if (i % 15 == 0)
                r[i - 1] = "FizzBuzz";
            else if (i % 3 == 0)
                r[i - 1] = "Fizz";
            else if (i % 5 == 0)
                r[i - 1] = "Buzz";
            else
                r[i - 1] = std::to_string(i);
        }
        return r;
    }
};

TEST_CASE {
    Solution s;

    assert(s.fizzBuzz(1) == vector<string>{"1"});
    assert(s.fizzBuzz(15) == (vector<string>{"1", "2", "Fizz", "4", "Buzz", "Fizz", "7", "8", "Fizz", "Buzz", "11", "Fizz", "13", "14", "FizzBuzz"}));
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
