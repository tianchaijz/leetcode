#include "common.h"

// Adding 1 to n - 1 elements is the same as subtracting 1 from one element,
// w.r.t goal of making the elements in the array equal.
// So, best way to do this is make all the elements in the array equal to the min element.
// sum(array) - n * minimum
class Solution {
  public:
    int minMoves(vector<int> &nums) {
        return accumulate(nums.begin(), nums.end(), 0L) - *min_element(nums.begin(), nums.end()) * nums.size();
    }
};

TEST_CASE {
    Solution s;
    vector<int> v{1, 2, 3};

    assert(s.minMoves(v) == 3);
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
