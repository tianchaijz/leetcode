#include "common.h"

class Solution {
  public:
    vector<int> largestValues(TreeNode *root) {
        vector<int> r;
        if (root == nullptr)
            return r;

        queue<TreeNode *> q;
        q.push(root);
        while (!q.empty()) {
            int max = INT_MIN;
            for (size_t i = q.size(); i != 0; --i) {
                auto n = q.front();
                q.pop();
                if (n->val > max)
                    max = n->val;
                if (n->left)
                    q.push(n->left);
                if (n->right)
                    q.push(n->right);
            }
            r.push_back(max);
        }

        return r;
    }
};

TEST_CASE {
    Solution s;

    TreeNode a(1), b(2), c(3), d(4), e(5), f(6);
    a.left = &c, a.right = &b;
    c.left = &e, c.right = &d;
    b.right = &f;

    assert(s.largestValues(&a) == (vector<int>{1, 3, 6}));
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
