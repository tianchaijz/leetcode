#include "common.h"

// Given a binary tree, find the leftmost value in the last row of the tree.
class Solution {
  public:
    int findBottomLeftValue(TreeNode *root) {
        pair<TreeNode *, int> r{root, 1};
        queue<decltype(r)> q;
        q.push(r);
        while (!q.empty()) {
            auto p = q.front();
            q.pop();
            if (p.second > r.second)
                r = p;
            if (p.first->left)
                q.push({p.first->left, p.second + 1});
            if (p.first->right)
                q.push({p.first->right, p.second + 1});
        }

        return r.first->val;
    }
};

class Solution1 {
  public:
    int findBottomLeftValue(TreeNode *root) {
        queue<TreeNode *> q;
        q.push(root);
        while (!q.empty()) {
            root = q.front();
            q.pop();
            if (root->right)
                q.push(root->right);
            if (root->left)
                q.push(root->left);
        }

        return root->val;
    }
};

TEST_CASE {
    Solution s;

    TreeNode a(1), b(2), c(3, &a, &b), d(4), e(5), f(6), g(7), h(8), i(9);

    assert(s.findBottomLeftValue(&c) == 1);

    a.left = &b, a.right = &c;
    b.left = &d;
    c.left = &e, c.right = &f;
    f.left = &g;

    assert(s.findBottomLeftValue(&a) == 7);

    g.right = &h;
    h.right = &i;

    assert(s.findBottomLeftValue(&g) == 9);
}

int main(int argc, char *argv[]) {
    TEST(Solution);
    TEST(Solution1);

    return 0;
}
