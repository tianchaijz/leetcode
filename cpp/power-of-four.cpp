#include "common.h"

class Solution {
  public:
    bool isPowerOfFour(int num) {
        while (num > 0 && num % 4 == 0)
            num >>= 2;
        return num == 1;
    }
};

class Solution1 {
  public:
    bool isPowerOfFour(int num) {
        return num > 0 && (num & (num - 1)) == 0 && (num - 1) % 3 == 0;
    }
};

TEST_CASE {
    Solution s;

    assert(s.isPowerOfFour(0) == false);
    assert(s.isPowerOfFour(1) == true);
    assert(s.isPowerOfFour(4) == true);
    assert(s.isPowerOfFour(5) == false);
    assert(s.isPowerOfFour(16) == true);
}

int main(int argc, char *argv[]) {
    TEST(Solution);
    TEST(Solution1);

    return 0;
}
