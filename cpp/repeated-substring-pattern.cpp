#include "common.h"

class Solution {
  public:
    bool repeatedSubstringPattern(string s) {
        int size = s.size(), n = maxRepeated(s);
        if (n < 2)
            return false;
        for (int groups = 2; groups <= n; ++groups) {
            if (n % groups == 0 && repeated(s, size / groups))
                return true;
        }

        return false;
    }

  private:
    bool repeated(string s, int n) {
        if (n == 0)
            return true;

        int m = s.size();
        if (m < n)
            return false;

        for (int i = n; i < m; i += n) {
            int j = 0;
            for (; j < n && s[j] == s[i + j]; ++j)
                ;
            if (j < n)
                return false;
        }

        return true;
    }

    int maxRepeated(string s) {
        int g = 0;
        int c[26] = {0};
        for (char a : s) {
            g = ++c[a - 'a'];
        }

        for (int n : c) {
            if (n)
                g = gcd(g, n);
        }

        return g;
    }

    int gcd(int a, int b) {
        while (b != 0) {
            int t = b;
            b = a % b;
            a = t;
        }

        return a;
    }
};

class Solution1 {
  public:
    bool repeatedSubstringPattern(string s) {
        int size = s.size();
        for (int len = size / 2; len >= 1; --len) {
            if (size % len == 0) {
                string sub = s.substr(0, len);
                int i, n = size / len;
                for (i = 1; i < n; ++i) {
                    if (s.substr(i * len, len) != sub)
                        break;
                }

                if (i == n)
                    return true;
            }
        }

        return false;
    }
};

class Solution2 {
  public:
    bool repeatedSubstringPattern(string s) {
        int n = s.size();
        for (int i = 1; i <= n / 2; ++i) {
            if (n % i == 0 && s.substr(i) == s.substr(0, n - i))
                return true;
        }
        return false;
    }
};

class Solution3 {
  public:
    bool repeatedSubstringPattern(string s) {
        if (s == "")
            return false;

        string sb = s + s;
        sb = sb.substr(1, sb.size() - 2);

        return sb.find(s) != string::npos;
    }
};

// KMP
class Solution4 {
  public:
    bool repeatedSubstringPattern(string s) {
        vector<int> pi(s.size());
        for (int j = 0, i = 1; i < s.size(); ++i) {
            for (; j && s[i] != s[j]; j = pi[j - 1])
                ;
            if (s[i] == s[j])
                ++j;
            pi[i] = j;
        }

        return pi.back() && s.size() % (s.size() - pi.back()) == 0;
    }
};

TEST_CASE {
    Solution s;

    assert(s.repeatedSubstringPattern("a") == false);
    assert(s.repeatedSubstringPattern("aa") == true);
    assert(s.repeatedSubstringPattern("aaa") == true);
    assert(s.repeatedSubstringPattern("abab") == true);
    assert(s.repeatedSubstringPattern("aabaaba") == false);
    assert(s.repeatedSubstringPattern("abababaaba") == false);
    assert(s.repeatedSubstringPattern("abcabcabcabc") == true);
    assert(s.repeatedSubstringPattern(string(10000, 'a')) == true);
}

int main(int argc, char *argv[]) {
    TEST(Solution);
    TEST(Solution1);
    TEST(Solution2);
    TEST(Solution3);
    TEST(Solution4);

    return 0;
}
