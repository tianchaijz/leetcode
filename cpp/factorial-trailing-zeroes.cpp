#include "common.h"

// https://discuss.leetcode.com/topic/6848/my-explanation-of-the-log-n-solution/2
// = 2 * 3 * ... * 5 * ... * 10 * ... 15 * ... * 25 * ... * 50 * ... * 125 * ... * 250 * ...
// = 2 * 3 * ... * 5 * ... * (5^1*2) * ... * (5^1*3) * ... * (5^2*1) * ... * (5^2*2) * ... * (5^3*1) * ... * (5^3*2) * ...
// Since multiple of 2 is more than multiple of 5, the number of zeros is dominant by the number of 5.
// return n/5 + n/25 + n/125 + n/625 + n/3125 + ...
class Solution {
  public:
    int trailingZeroes(int n) {
        return n == 0 ? 0 : n / 5 + trailingZeroes(n / 5);
    }
};

TEST_CASE {
    Solution s;

    assert(s.trailingZeroes(5) == 1);
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
