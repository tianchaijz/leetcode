#include "common.h"

class Solution {
  public:
    int arrangeCoins(int n) {
        int i = 0;
        do {
            ++i;
            n -= i;
        } while (n >= 0);

        return i - 1;
    }
};

class Solution1 {
  public:
    int arrangeCoins(int n) {
        if (n < 0)
            return 0;

        int lo = 0, hi = n;
        while (lo <= hi) {
            int mid = lo + (hi - lo) / 2;
            if (n < (1L + mid) * mid / 2)
                hi = mid - 1;
            else
                lo = mid + 1;
        }

        return lo - 1;
    }
};

class Solution2 {
  public:
    int arrangeCoins(int n) {
        return sqrt(2. * n + 0.25) - 0.5;
    }
};

TEST_CASE {
    Solution s;

    assert(s.arrangeCoins(0) == 0);
    assert(s.arrangeCoins(1) == 1);
    assert(s.arrangeCoins(2) == 1);
    assert(s.arrangeCoins(8) == 3);
    assert(s.arrangeCoins(1804289383) == 60070);
}

int main(int argc, char *argv[]) {
    TEST(Solution);
    TEST(Solution1);
    TEST(Solution2);

    return 0;
}
