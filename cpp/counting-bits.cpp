#include "common.h"

class Solution {
  public:
    vector<int> countBits(int num) {
        vector<int> r(num + 1, 0);
        for (int i = 1; i <= num; ++i) {
            r[i] = r[i & (i - 1)] + 1;
        }

        return r;
    }
};

class Solution1 {
  public:
    vector<int> countBits(int num) {
        vector<int> r(num + 1, 0);
        // r[i] = r[i / 2] + i % 2
        for (int i = 1; i <= num; ++i) {
            r[i] = r[i >> 1] + (i & 1);
        }

        return r;
    }
};

TEST_CASE {
    Solution s;

    assert(s.countBits(5) == (vector<int>{0, 1, 1, 2, 1, 2}));
}

int main(int argc, char *argv[]) {
    TEST(Solution);
    TEST(Solution1);

    return 0;
}
