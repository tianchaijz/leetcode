#include "common.h"

class Solution {
  public:
    vector<int> findDisappearedNumbers(vector<int> &nums) {
        vector<int> r;

        for (int i : nums) {
            int idx = abs(i) - 1;
            nums[idx] = -1 * abs(nums[idx]);
        }

        for (int i = 0; i < nums.size(); ++i) {
            if (nums[i] > 0)
                r.emplace_back(i + 1);
        }

        return r;
    }
};

TEST_CASE {
    Solution s;
    vector<int> v{4, 3, 2, 7, 8, 2, 3, 1};

    assert(s.findDisappearedNumbers(v) == (vector<int>{5, 6}));
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
