#include "common.h"

class Solution {
  public:
    bool isPalindrome(ListNode *head) {
        if (head == nullptr || head->next == nullptr)
            return true;

        ListNode *slow = head, *fast = head;
        while (fast->next && fast->next->next) {
            fast = fast->next->next;
            slow = slow->next;
        }

        slow = reverseList(slow->next);
        while (slow && head->val == slow->val) {
            head = head->next;
            slow = slow->next;
        }

        return !slow;
    }

  private:
    ListNode *reverseList(ListNode *head) {
        ListNode *tail = nullptr, *t;
        while (head) {
            t = head->next;
            head->next = tail;
            tail = head;
            head = t;
        }

        return tail;
    }
};

class Solution1 {
  public:
    bool isPalindrome(ListNode *head) {
        if (head == nullptr || head->next == nullptr)
            return true;

        ListNode *slow = head, *fast = head, *back = nullptr, *t;
        while (fast && fast->next) {
            fast = fast->next->next;
            t = slow->next;
            slow->next = back;
            back = slow;
            slow = t;
        }

        if (fast)
            slow = slow->next;

        while (back && back->val == slow->val) {
            slow = slow->next;
            back = back->next;
        }

        return !back;
    }
};

TEST_CASE {
    Solution s;

    ListNode a(1);
    ListNode b(2, &a);
    ListNode c(1, &b);

    ListNode d(1);
    ListNode e(2, &d);
    ListNode f(2, &e);
    ListNode g(1, &f);

    assert(s.isPalindrome(&a));
    assert(s.isPalindrome(&c));
    assert(s.isPalindrome(&g));
}

int main(int argc, char *argv[]) {
    TEST(Solution);
    TEST(Solution1);

    return 0;
}
