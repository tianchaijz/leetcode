#include "common.h"

class Solution {
  public:
    bool isUgly(int num) {
        if (num) {
            while (num % 2 == 0)
                num /= 2;
            while (num % 3 == 0)
                num /= 3;
            while (num % 5 == 0)
                num /= 5;
        }

        return num == 1;
    }
};

class Solution1 {
  public:
    bool isUgly(int num) {
        for (int i = 2; i < 6 && num; ++i)
            while (num % i == 0)
                num /= i;
        return num == 1;
    }
};

TEST_CASE {
    Solution s;

    assert(s.isUgly(0) == false);
    assert(s.isUgly(1) == true);
    assert(s.isUgly(2) == true);
    assert(s.isUgly(3) == true);
    assert(s.isUgly(5) == true);
    assert(s.isUgly(14) == false);
    assert(s.isUgly(15) == true);
}

int main(int argc, char *argv[]) {
    TEST(Solution);
    TEST(Solution1);

    return 0;
}
