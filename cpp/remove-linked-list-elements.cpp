#define TEST_CALLS 1
#include "common.h"

class Solution {
  public:
    ListNode *removeElements(ListNode *head, int val) {
        ListNode **pp = &head;
        while (*pp) {
            if ((*pp)->val == val) {
                delete *pp;
                *pp = (*pp)->next; // have the list skip this entry
            } else {
                pp = &(*pp)->next; // move to the next entry
            }
        }

        return head;
    }
};

TEST_CASE {
    Solution s;

    ListNode *head = createList(1);
    printList(head);
    head = s.removeElements(head, 0);
    printList(head);

    head = createList(2);
    printList(head);
    head = s.removeElements(head, 1);
    printList(head);
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
