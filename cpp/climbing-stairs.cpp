#include "common.h"

class Solution {
  public:
    int climbStairs(int n) {
        if (n < 3)
            return n;

        int a = 1, b = 2, c;
        while (n-- > 2) {
            c = a + b;
            a = b;
            b = c;
        }

        return c;
    }
};

// https://discuss.leetcode.com/topic/17002/3-4-short-lines-in-every-language
class Solution1 {
  public:
    int climbStairs(int n) {
        int a = 1, b = 1;
        while (n--)
            a = (b += a) - a;

        return a;
    }
};

TEST_CASE {
    Solution s;
}

int main(int argc, char *argv[]) {
    TEST(Solution);

    return 0;
}
