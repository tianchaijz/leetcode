#include "common.h"

class Solution {
  public:
    string longestCommonPrefix(vector<string> &strs) {
        if (strs.empty())
            return "";
        for (size_t i = 0;; ++i) {
            char c = strs[0][i];
            for (size_t j = 0; j < strs.size(); ++j) {
                if (!strs[j][i] || strs[j][i] != c)
                    return strs[0].substr(0, i);
            }
        }
    }
};

class Solution1 {
  public:
    string longestCommonPrefix(vector<string> &strs) {
        if (strs.empty())
            return "";
        sort(strs.begin(), strs.end());
        string a = strs.front(), b = strs.back();
        for (size_t i = 0; i < a.size() && i < b.size(); ++i) {
            if (a[i] != b[i])
                return a.substr(0, i);
        }

        return a;
    }
};

TEST_CASE {
    Solution s;

    vector<string> strs{"ab", "abc", "abd"};
    vector<string> strs1{"ab", "ac", "ad"};
    vector<string> strs2{"ab", "abc", "abd"};
    vector<string> strs3{"ab", "c", "d"};

    assert(s.longestCommonPrefix(strs) == "ab");
    assert(s.longestCommonPrefix(strs1) == "a");
    assert(s.longestCommonPrefix(strs2) == "ab");
    assert(s.longestCommonPrefix(strs3) == "");
}

int main(int argc, char *argv[]) {
    TEST(Solution);
    TEST(Solution1);

    return 0;
}
