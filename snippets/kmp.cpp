// http://algs4.cs.princeton.edu/53substring/

#include <cassert>
#include <iostream>
#include <string>
#include <vector>

using std::string;
using std::vector;

class kmp {
  public:
    kmp(string pattern) : pattern(pattern) {
        assert(pattern.size() > 0);
        dfa = new int *[pattern.size()];
        for (int i = 0; i < pattern.size(); ++i) {
            dfa[i] = new int[256];
        }
        initialize();
    }

    kmp(const kmp &) = delete;
    void operator=(const kmp &) = delete;

    ~kmp() {
        for (int i = 0; i < pattern.size(); ++i)
            delete[] dfa[i];
        delete[] dfa;
    }

    int search(string s) {
        int m = pattern.size(), n = s.size();
        int state, i;
        for (state = 0, i = 0; state < m && i < n; ++i)
            state = dfa[state][s[i] - '\0'];
        if (state == m)
            return i - m; // found
        return n;         // not found
    }

  private:
    // create KMP DFA from pattern
    inline void initialize() {
        int start = 0;
        dfa[start][pattern[0] - '\0'] = 1;
        for (int state = 1; state < pattern.size(); ++state) {
            for (int c = 0; c < 256; ++c)
                dfa[state][c] = dfa[start][c];             // copy mismatch cases
            dfa[state][pattern[state] - '\0'] = state + 1; // set match case
            start = dfa[start][pattern[state] - '\0'];     // update restart state
        }
    }

    string pattern;
    int **dfa;
};

class kmp_nfa {
  public:
    kmp_nfa(string pattern) : pattern(pattern) {
        assert(pattern.size() > 0);
        next = vector<int>(pattern.size(), 0);
        initialize();
    }

    kmp_nfa(const kmp_nfa &) = delete;
    void operator=(const kmp_nfa &) = delete;

    ~kmp_nfa() {
    }

    // simulate the NFA to find match
    int search(string s) {
        int m = pattern.size(), n = s.size();
        int state, i;
        for (state = 0, i = 0; state < m && i < n; ++i) {
            for (; state && s[i] != pattern[state]; state = next[state - 1])
                ;
            if (s[i] == pattern[state])
                ++state;
        }
        if (state == m)
            return i - m;
        return n;
    }

  private:
    // create KMP NFA from pattern
    inline void initialize() {
        for (int state = 0, i = 1; i < pattern.size(); ++i) {
            // state > 0 and not equal, find previous match position
            for (; state && pattern[i] != pattern[state]; state = next[state - 1])
                ;
            next[i] = pattern[i] == pattern[state] ? ++state : 0;
        }
    }

    string pattern;
    vector<int> next;
};

int main(int argc, char *argv[]) {
    kmp k("abc");
    kmp k1("abcabcabc");

    kmp_nfa kn("abc");
    kmp_nfa kn1("abcabcabc");
    kmp_nfa kn2("abcdabd");

    std::cout << k.search("123456789");
    std::cout << k.search("123456abc");
    std::cout << k.search("123abc456");
    std::cout << k.search("abc123456");
    std::cout << k.search("xbc123456");
    std::cout << k1.search("abcabc123abcabc4abcabcabc") << std::endl;

    std::cout << kn.search("123456789");
    std::cout << kn.search("123456abc");
    std::cout << kn.search("123abc456");
    std::cout << kn.search("abc123456");
    std::cout << kn.search("xbc123456");
    std::cout << kn1.search("abcabc123abcabc4abcabcabc") << std::endl;

    kmp_nfa kn3("ABCDABD");
    std::cout << kn3.search("1ABCDABD") << std::endl;

    return 0;
}
