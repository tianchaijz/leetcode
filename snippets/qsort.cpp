#include <cassert>
#include <iostream>
#include <stdio.h>
#include <utility>
#include <vector>

template <typename T>
std::ostream &operator<<(std::ostream &os, const std::vector<T> &vec) {
    for (auto &el : vec) {
        os << el << " ";
    }

    os << std::endl;

    return os;
}

void qsort(std::vector<int> &v, int lo, int hi) {
    if (lo >= hi)
        return;

    int i = lo, j = hi + 1;
    int pivot = v[lo];
    while (i < j) {
        while (i < hi && v[++i] < pivot)
            ;
        while (j > lo && v[--j] > pivot)
            ;
        if (i < j)
            // can not do ++, --, since next loop will do it again
            std::swap(v[i], v[j]);
    }

    if (j != lo)
        std::swap(v[lo], v[j]);

    qsort(v, lo, j - 1);
    qsort(v, j + 1, hi);
}

template <typename T>
void test(T &&v) {
    T a = v;

    std::sort(v.begin(), v.end());

    qsort(a, 0, a.size() - 1);
    assert(a == v);
}

int main(int argc, char *argv[]) {
    std::srand(std::time(0));

    for (int i = 0; i < 500000; ++i) {
        std::vector<int> v(std::rand() % 6 + 1);
        std::generate(v.begin(), v.end(), [] { return std::rand() % 4; });

        test(v);
    }

    for (int i = 0; i < 500000; ++i) {
        std::vector<int> v(std::rand() % 10 + 1);
        std::generate(v.begin(), v.end(), [] { return std::rand() % 10; });

        test(v);
    }

    test(std::vector<int>{2, 2, 1, 3});
    test(std::vector<int>{2, 2, -1, 3});
    test(std::vector<int>{2, 2, -1, 1});
    test(std::vector<int>{1, 2});
    test(std::vector<int>{2, 1});

    return 0;
}
