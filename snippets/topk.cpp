#include <deque>
#include <iostream>
#include <queue>
#include <stack>
#include <vector>

#define K 3

int main(int argc, char *argv[]) {
    std::priority_queue<int, std::vector<int>, std::greater<int>> pq;
    int num;

    while (std::cin >> num) {
        pq.push(num);
        if (pq.size() > K)
            pq.pop();
    }

    std::stack<int, std::deque<int>> stack;
    while (!pq.empty()) {
        stack.push(pq.top());
        pq.pop();
    }

    while (!stack.empty()) {
        std::cout << stack.top() << std::endl;
        stack.pop();
    }

    return 0;
}
