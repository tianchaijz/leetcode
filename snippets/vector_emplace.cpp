#include <iostream>
#include <vector>

class A {
  public:
    A(int n) : num(n) {
        std::cout << "A(n)" << std::endl;
    }

    A() : num(0) {
        std::cout << "A()";
    }

    A(const A &rhs) noexcept {
        num = rhs.num;
        std::cout << "A(A &)" << std::endl;
    }

    A(A &&rhs) noexcept {
        num = rhs.num;
        std::cout << "A(A &&)" << std::endl;
    }

  private:
    int num;
};

int main() {
    {
        std::vector<A> v;
        std::cout << "emplace_back:" << std::endl;
        v.emplace_back(0);
    }

    {
        std::vector<A> v;
        std::cout << "push_back:" << std::endl;
        v.push_back(1);
    }

    return 0;
}
