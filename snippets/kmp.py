import os
from graphviz import Digraph


class KMP:
    def __init__(self, pattern):
        self.next = [0] * len(pattern)
        self.pattern = pattern
        self.dot = None

        self.initialize()

    def initialize(self):
        state, pattern = 0, self.pattern
        for i in range(1, len(self.pattern)):
            state = self.next[i - 1]
            while(state and pattern[i] != pattern[state]):
                state = self.next[state - 1]
            self.next[i] = state + 1 if pattern[i] == pattern[state] else 0

    def view(self):
        size = len(self.pattern)

        self.dot = Digraph(comment='NFA', graph_attr={'rankdir': 'LR'})
        self.dot.node(self.pattern, _attributes={'shape': 'rect',
                                                 'color': 'red',
                                                 'label': self.pattern})
        # accept state
        self.dot.node(str(size),
                      _attributes={'shape': 'doublecircle',
                                   'color': 'blue',
                                   'label': str(size)})
        for i in range(size):
            if i == 0:
                self.dot.node(str(i),
                              _attributes={'shape': 'doublecircle',
                                           'label': str(i)})
            else:
                self.dot.node(str(i),
                              _attributes={'shape': 'circle',
                                           'label': str(i)})
            self.dot.edge(str(i), str(i + 1), self.pattern[i])
            if i and self.next[i - 1]:
                self.dot.edge(str(i), str(self.next[i - 1]))

        self.dot.render(self.pattern + '.dot')
        os.system('dot {0}.dot -Tpng -o {0}.png'.format(self.pattern))

    def search(self, s):
        state, pattern = 0, self.pattern
        m, n = len(self.pattern), len(s)
        find = []
        for i in range(n):
            while(state and s[i] != pattern[state]):
                state = self.next[state - 1]
            if s[i] == pattern[state]:
                state += 1
                if state == m:
                    find.append(i + 1 - state)
                    state = 0
        return find


def main():
    KMP("ABCDABD").view()
    KMP("aaa").view()
    KMP("abcabcabc").view()


if __name__ == "__main__":
    main()
