#include <stdio.h>

int main(void) {
    char ch = -1;

    // char 用作数组下标时，一定要先转 unsigned char （因为 char 通常是有符号的）。
    // 不能直接转 int 或 unsigned int ，会数组下标越界。
    printf("%d %u %d\n", (int)ch, (unsigned)ch, (unsigned char)ch);
    return 0;
}
