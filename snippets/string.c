// https://opensource.apple.com/source/Libc/Libc-167/gen.subproj/ppc.subproj/strncmp.c
// https://opensource.apple.com/source/Libc/Libc-320/string/FreeBSD/strcmp.c
// https://opensource.apple.com/source/xnu/xnu-344/libsa/strstr.c

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int strncmp(const char *s1, const char *s2, size_t n) {
    for (; n > 0; ++s1, ++s2, --n) {
        if (*s1 != *s2)
            return ((*(unsigned char *)s1 < *(unsigned char *)s2) ? -1 : 1);
        else if (*s1 == '\0')
            return 0;
    }

    return 0;
}

int strcmp(const char *s1, const char *s2) {
    for (; *s1 == *s2; ++s1, ++s2)
        if (*s1 == '\0')
            return 0;
    return ((*(unsigned char *)s1 < *(unsigned char *)s2) ? -1 : 1);
}

char *strstr(const char *in, const char *str) {
    char c;
    size_t len;

    c = *str++;
    if (!c)
        return (char *)in; // Trivial empty string case

    len = strlen(str);
    do {
        char sc;
        do {
            sc = *in++;
            if (!sc)
                return (char *)0;
        } while (sc != c);
    } while (strncmp(in, str, len) != 0);

    return (char *)(in - 1);
}

int main(int argc, char *argv[]) {
    printf("%d\n", strncmp("1234", "123", 3));
    printf("%d\n", strncmp("123", "1234", 3));
    printf("%d\n", strncmp("223", "1234", 3));
    printf("%d\n", strncmp("123", "2234", 3));

    printf("\n");
    printf("%d\n", strcmp("123", "123"));
    printf("%d\n", strcmp("123", "1234"));
    printf("%d\n", strcmp("1234", "123"));
    printf("%d\n", strcmp("223", "123"));
    printf("%d\n", strcmp("123", "223"));

    printf("%s\n", strstr("abc123def", "123"));

    return 0;
}
